#!/usr/bin/env bash
#
# packageManagerCF.sh - Common functions for package managers and similar functions
#
# Changes:
# v1.5.1
# - Fixed a logic error in determinePM()
#
# v1.5.0
# - Added pamac to list of package managers
# - Made pacaur a seperate manager, now must be installed manually
# - Removed all references to Snap, one of many bad decisions from Canonical I now understand
# - Updated all statements to use eval for safer excecution
# - Changed pipManage to use 'sudo -H python3 -m pip' instead of pip3
#
# v1.4.4
# - Small updates to modernize script
#
# v1.4.3
# - Updated all debug() calls to conform with commonFunctions.sh v2.0.0
# - Made variables in each function local for better control
# - More verbosity in most functions debug() calls
#
# v1.4.2
# - Changed functions to use 'apt' instead of 'apt-get' for stability
#
# v1.4.1
# - Updated all functions to use pip, easiest implementation I could figure out
# - pip will now update all packages if none are given
# 
# v1.4.0
# - Added pipManage()
# - Used to install, remove, and update Python packages using pip3
#
# v1.3.4
# - Forgot to add sudo to cleanPM() and upgradePM(), fixed
#
# v1.3.3
# - Fixed a logic error
# - Corrected check for pacaur
#
# v1.3.2
# - Fixed debug messages
# - Added return values to necessary functions for pm.sh
# - Added snapd capabilities for Ubuntu/Debian systems
# - Small functional change to checkRequirements()
#
# v1.3.1
# - Turns out yaourt is not recommended. Changed all functions to use pacaur instead
#
# v1.3.0
# - Added distUpgradePM() for full distributions upgrades
# - Fixed apt-get in upgradePM() accordingly
# - Updated all functions to use yaourt exclusively from now on (personal decision)
# - Removed warning for cleaning Arch systems, turns out I didn't know what it did
#
# v1.2.3
# - Updating changelog to new settings
#
# v1.2.2
# - Changed updatePM() to only update pacman, no need to update twice with yaourt!
# - Update: *Reaper voice* Didn't take
#
# v1.2.1
# - Small change to checkRequirements for non-sudo scripts
# - checkRequirements now uses pm.sh to install programs, turns out sudo-ing functions is difficult
#
# v1.2.0
# - All programs now support the -o|--options from pm.sh
# - Package managers are no longer non-interactive by default
# - Fixed a bug that was keeping yaourt from working...
#
# v1.1.2
# - Foiled by a bang!
#
# v1.1.1
# - Changed a couple debug calls
#
# v1.1.0
# - That was a quick major version... Moved checkRequirements() to this script from cF.sh
# - Added a recursive call for sourcing commonFunctions, just in case...
# - Fixed SC1048
#
# v1.0.0
# - First release version
# - Added completed pkgInfo()
# - Added failsafes to all functions
# - determinePM() will now state if no package manager was found, then quit
# - Added as close to an '#ifndef' statement as I could for sourcing this script
#
# TODO:
# - Add npm and npm to install(), update(), upgrade(), remove(), and possibly query()/info() - but NOT to determinePM()
# - For all functions - add ability to 'run PM as $1' if there is an argument
#   ~ e.g. "upgradePM" will upgrade the current PM, "upgradePM npm" will (attempt to) upgrade npm
# - Change scripts to allow sending package names to upgradePM specifically for pip
#
# v1.5.1, 05 Dec. 2021, 02:56 PST

### Setup

if [[ -n $pmCFvar ]]; then
	return 0
fi

### Variables

export pmCFvar=0 # Ignore shellcheck saying this isn't used. Lets script know if this has been sourced or not.
export pmOptions="" # Options to be added before running any package manager
export rval=0 # Just to make sure 

### Functions
if [[ -n $cfVar ]]; then
	if ! source /usr/share/commonFunctions.sh; then
		if ! source commonFunctions.sh; then
			echo "ERROR: commonFunctions.sh not found! Please install from https://gitlab.com/mrmusic25/linux-pref and re-run script!" 1>&2
			exit 1
		fi
	fi
fi

## determinePM()
# Function: Determine package manager (PM) and export for script use
# PreReq: None
#
# Call: determinePM
#
# Input: No input necessary; ignored
#
# Output: Exports variable 'program'; no return value (yet)
#
# Other info: Updates repositories if possible, redirect to /dev/null if you don't want to see it
# https://linuxconfig.org/comparison-of-major-linux-package-management-systems
function determinePM() {
	if [[ -z $program || "$program" != "NULL" ]]; then
		return 0
	elif [[ ! -z $(which apt 2>/dev/null) ]]; then # Most common, so it goes first
		export program="apt"
		#apt-get update
	elif [[ ! -z $(which dnf 2>/dev/null) ]]; then # This is why we love DistroWatch, learned about the 'replacement' to yum!
		export program="dnf"
		#dnf check-update
	elif [[ ! -z $(which yum 2>/dev/null) ]]; then
		export program="yum"
		#yum check-update
	elif [[ ! -z $(which pamac 2>/dev/null) ]]; then
		export program="pamac"
	elif [[ ! -z $(which pacaur 2>/dev/null) ]]; then
		export program="pacaur"
	elif [[ ! -z $(which pacman 2>/dev/null) ]]; then
		export program="pacman"
	elif [[ ! -z $(which slackpkg 2>/dev/null) ]]; then
		export program="slackpkg"
		#slackpkg update
	elif [[ ! -z $(which zypper 2>/dev/null) ]]; then # Main PM for openSUSE
		export program="zypper" 
		# https://en.opensuse.org/SDB:Zypper_usage for more info
	elif [[ ! -z $(which emerge 2>/dev/null) ]]; then # Portage, PM for Gentoo (command is emerge)
		export program="emerge"
	elif [[ ! -z $(which rpm 2>/dev/null) ]]; then
		export program="rpm"
		#rpm -F --justdb # Only updates the DB, not the system
	else
		debug "l3" "Package manager not found! Please contact script maintainter!"
		exit 1
	fi
	debug "l5" "Package manager found! $program"
	return 0
}

## updatePM()
#
# Function: Update the package manager's database(s)
# PreReq: $program must be set OR determinePM() must be run first
#
# Call: updatePM
#
# Input: None
#
# Output: stdout
#
# Other info: None, simple function
function updatePM() {
	# Check to make sure $program is set
	if [[ -z $program || "$program" == "NULL" ]]; then
		debug "l2" "Attempted to update package manager without setting program! Fixing..."
		determinePM
	fi
	
	# Now, do the proper update command
	debug "l1" "Refreshing the package manager's database"
	case $program in
		apt)
		eval "sudo apt $pmOptions update"
		# No update option for snapd
		;;
		pamac)
		eval "sudo pamac checkupdates $pmOptions"
		;;
		pacaur)
		eval "pacaur $pmOptions -Syy"
		;;
		pacman)
		eval "pacman $pmOptions -Syy"
		;;
		dnf)
		eval "sudo dnf $pmOptions check-update"
		;;
		yum)
		eval "sudo yum $pmOptions check-update"
		;;
		slackpkg)
		eval "sudo slackpkg $pmOptions update"
		;;
		emerge)
		eval "sudo emerge-webrsync $pmOptions"
		;;
		rpm)
		eval "sudo rpm $pmOptions -F --justdb" # Only updates the DB, not the system
		;;
		zypper)
		eval "sudo zypper $pmOptions refresh"
		;;
		pip)
		true # No way to manually refresh pip repo
		;;
		*)
		debug "l3" "Unsupported package manager detected! Please contact script maintainer to get yours added to the list!"
		return 1
		;;
	esac
	return $?
}

## universalInstaller()
# Function: Attempt to install all programs listed in called args
# PreReq: Must run determinePM() beforehand, or have $program set to a valid option
#
# Call: universalInstaller <program1> [program2] [program3] ...
#
# Input: As many arguments as you want, each one a valid package name
#
# Output: stdout, no return values at this time
#
# Other info: Loops through arguments and installs one at a time, encapsulate in ' ' if you want some installed together
#             e.g. universalInstaller wavemon gpm 'zsh ssh' -> wavemon and gpm installed by themselves, zsh and ssh installed together
function universalInstaller() {
	# Check to make sure $program is set
	if [[ -z $program || "$program" == "NULL" ]]; then
		debug "Attempted to run universalInstaller() without setting program! Fixing..."
		determinePM
	fi
	
	local rval=0 # Return value
	local v=0

	for var in "$@" 
	do
		debug "l1" "Attempting to install program: $var"
		case $program in
			apt)
			eval "sudo apt $pmOptions install $var" 
			((rval+=$?))
			;;
			dnf)
			eval "sudo dnf $pmOptions install $var"
			((rval+=$?))
			;;
			yum)
			eval "sudo yum $pmOptions install $var"
			((rval+=$?))
			;;
			slackpkg)
			eval "sudo slackpkg $pmOptions install $var"
			((rval+=$?))
			;;
			zypper)
			eval "sudo zypper $pmOptions install $var"
			((rval+=$?))
			;;
			rpm)
			eval "sudo rpm $pmOptions -i $var" 
			((rval+=$?))
			;;
			emerge)
			eval "sudo emerge $pmOptions $var"
			((rval+=$?))
			;;
			pamac)
			eval "sudo pamac install $pmOptions $var"
			v=$?
			((rval+=$v))
			if [[ $v -ne 0 ]]; then
				debug "l2" "pamac could not install from repository $var, trying to build from AUR!"
				eval "pamac build $pmOptions $var" # Don't build as root
				((rval+=$v))
			fi
			;;
			pacaur)
			eval "pacaur -S $var"
			((rval+=$?))
			;;
			pacman)
			eval "sudo pacman $pmOptions -S $var"
			((rval+=$?))
			;;
			pip)
			pipManage "$var"
			((rval+=$?))
			;;
			*)
			debug "l3" "Unsupported package manager detected! Please contact script maintainer to get yours added to the list!"
			return 1
			;;
		esac
	done

	return "$rval"
}

## upgradePM()
#
# Function: Upgrade all packages in the system with newer version available
# PreReq: $program must be set OR determinePM() must be run first
#
# Call: upgradePM
#
# Input: None
#
# Output: stdout
#
# Other info: None, simple function
function upgradePM() {
	# Check to make sure $program is set
	if [[ -z $program || "$program" == "NULL" ]]; then
		debug "l2" "Attempted to upgrade package manager without setting program! Fixing..."
		determinePM
	fi
	local rval=0
	
	debug "Preparing to upgrade system using $program"
	case $program in
		apt)
		eval "sudo apt $pmOptions full-upgrade"
		((rval+=$?))
		;;
		dnf)
		eval "sudo dnf $pmOptions upgrade"
		((rval+=$?))
		;;
		yum)
		eval "sudo yum $pmOptions upgrade"
		((rval+=$?))
		;;
		slackpkg)
		eval "sudo slackpkg $pmOptions install-new" # Required line
		((rval+=$?))
		eval "sudo slackpkg $pmOptions upgrade-all"
		((rval+=$?))
		;;
		zypper)
		eval "sudo zypper $pmOptions update"
		((rval+=$?))
		;;
		rpm)
		eval "sudo rpm $pmOptions -F"
		((rval+=$?))
		;;
		pamac)
		eval "sudo pamac update $pmOptions"
		((rval+=$?))
		;;
		pacaur)
		eval "pacaur $pmOptions -Syu"
		((rval+=$?))
		;;
		pacman)
		eval "sudo pacman $pmOptions -Syu"
		((rval+=$?))
		;;
		emerge)
		eval "sudo emerge $pmOptions --sync --update --deep @world" # Gentoo is strange
		((rval+=$?))
		;;
		pip)
		pipManage update
		((rval+=$?))
		;;
		*)
		debug "l3" "Unsupported package manager detected! Please contact script maintainer to get yours added to the list!"
		return 1
		;;
	esac
	return "$rval"
}

## cleanPM()
#
# Function: Cleans the systme of stale packages and wasted space
# PreReq: $program must be set OR determinePM() must be run first
#
# Call: cleanPM
#
# Input: None
#
# Output: stdout
#
# Other info: Some functions do not have a clean option, it will notify the user if so
function cleanPM() {
	# Check to make sure $program is set
	if [[ -z $program || "$program" == "NULL" ]]; then
		debug "Attempted to clean package manager without setting program! Fixing..."
		determinePM
	fi
	local rval=0
	
	debug "INFO: Preparing to clean system with $program"
	case $program in
		apt)
		eval "sudo apt $pmOptions autoremove"
		((rval+=$?))
		eval "sudo apt-get $pmOptions autoclean"
		((rval+=$?))
		;;
		dnf)
		eval "sudo dnf $pmOptions clean all"
		((rval+=$?))
		eval "sudo dnf $pmOptions autoerase"
		((rval+=$?))
		;;
		yum)
		eval "sudo yum $pmOptions clean all"
		((rval+=$?))
		;;
		slackpkg)
		eval "sudo slackpkg $pmOptions clean-system"
		((rval+=$?))
		;;
		rpm)
		announce "RPM has no clean function"
		# Nothing to be done
		;;
		pamac)
		eval "sudo pamac clean $pmOptions"
		((rval+=$?))
		;;
		pacaur)
		eval "pacaur -Sc"
		((rval+=$?))
		;;
		pacman)
		eval "sudo pacman $pmOptions -Sc"
		((rval+=$?))
		;;
		zypper)
		announce "Zypper has no clean function"
		;;
		emerge)
		eval "sudo emerge $pmOptions --clean"
		((rval+=$?))
		eval "sudo emerge $pmOptions --depclean" # Couldn't tell which was the only one necessary, so I included both
		((rval+=$?))
		;;
		pip)
		true # No clean mechanism for pip
		;;
		*)
		debug "l3" "Unsupported package manager detected! Please contact script maintainer to get yours added to the list!"
		return 1
		;;
	esac
	return "$rval"
}

## queryPM()
#
# Function: Check for packages with a similar name in package database
# PreReq: $program must be set, or run determinePM() must be run first
#
# Call: queryPM <package_name> [package_name] ...
#
# Input: Will search and return info for each argument
#
# Output: stdout
#
# Other info: It will print which package it is looking for before displaying information
function queryPM() {
	# Check to make sure $program is set
	if [[ -z $program || "$program" == "NULL" ]]; then
		debug "l2" "Attempted to query package manager without setting program! Fixing..."
		determinePM
	fi
	
	local rval=0 # return value
	
	for var in "$@"
	do
		debug "l1" "Querying packagage database for: $var"
		case $program in
			apt)
			eval "sudo apt-cache $pmOptions search $var"
			((rval+=$?))
			;;
			pamac)
			eval "pamac search $pmOptions $var"
			((rval+=$?))
			;;
			pacaur)
			eval "pacaur $pmOptions -Ss $var" # Program automatically displays Arch-repo AND AUR programs
			((rval+=$?))
			;;
			pacman)
			eval "pacman $pmOptions -Ss $var"
			((rval+=$?))
			;;
			yum)
			eval "yum $pmOptions search $var" # Change to 'yum search all' if the results aren't good enough
			((rval+=$?))
			;;
			emerge)
			eval "emerge $pmOptions --search $var" # Like yum, use 'emerge --searchdesc' if the results aren't enough
			((rval+=$?))
			;;
			zypper)
			eval "zypper $pmOptions search $var"
			((rval+=$?))
			;;
			dnf)
			eval "dnf $pmOptions search $var"
			((rval+=$?))
			;;
			rpm)
			eval "rpm $pmOptions -q $var"
			((rval+=$?))
			;;
			slackpkg)
			eval "slackpkg $pmOptions search $var"
			((rval+=$?))
			;;
			pip)
			debug "l3" "Searching pip is not supported at this time!"
			return 0
			;;
			*)
			debug "l3" "Unsupported package manager detected! Please contact script maintainer to get yours added to the list!"
			return 1
			;;
		esac
	done
	return "$rval"
}

## removePM()
#
# Function: Remove given packages
# PreReq: Set $program, or run determinePM()
#
# Call: removePM <package_name> [package_name] ...
#
# Input: Names of packages to remove
#
# Output: stdout
#
# Other info: No --assume-yes, just in case. Pay attention!
function removePM() {
	# Check to make sure $program is set
	if [[ -z $program || "$program" == "NULL" ]]; then
		debug "l2" "Attempted to remove packages without setting program! Fixing..."
		determinePM
	fi
	local rval=0
	
	for var in "$@"
	do
		debug "l2" "Attempting to remove program $var..."
		case $program in
			apt)
			eval "sudo apt $pmOptions remove $var"
			((rval+=v))
			;;
			pamac)
			eval "sudo pamac remove $pmOptions $var"
			((rval+=$?))
			;;
			pacaur)
			eval "pacaur $pmOptions -R $var"
			((rval+=$?))
			;;
			pacman)
			eval "sudo pacman $pmOptions -R $var"
			((rval+=$?))
			;;
			yum)
			eval "sudo yum $pmOptions remove $var"
			((rval+=$?))
			;;
			emerge)
			eval "sudo emerge $pmOptions --remove --depclean $var"
			((rval+=$?))
			;;
			dnf)
			eval "sudo dnf $pmOptions remove $var"
			((rval+=$?))
			;;
			rpm)
			eval "sudo rpm $pmOptions -e $var"
			((rval+=$?))
			;;
			slackpkg)
			eval "sudo slackpkg $pmOptions remove $var"
			((rval+=$?))
			;;
			zypper)
			eval "sudo zypper $pmOptions remove $var"
			((rval+=$?))
			;;
			pip)
			pipManage remove "$var"
			((rval+=$?))
			;;
			*)
			debug "l3" "Unsupported package manager detected! Please contact script maintainer to get yours added to the list!"
			return 1
			;;
		esac
	done
	return "$rval"
}

## pkgInfo()
#
# Function: Display info about a package (dependencies, version, maintainer, etc)
# PreReq: Have $program set, or run determinePM()
#
# Call: pkgInfo <package_name> [package_name] ...
#
# Input: Package names
#
# Output: stdout
#
# Other info: None, simple function
function pkgInfo() {
	# Check to make sure $program is set
	if [[ -z $program || "$program" == "NULL" ]]; then
		debug "l2" "Attempted to remove packages without setting program! Fixing..."
		determinePM
	fi
	rval=0
	
	for var in "$@"
	do
		debug "l1" "Displaying package info of: $var"
		case "$program" in
			pamac)
			eval "pamac info $pmOptions $var"
			((rval+=$?))
			;;
			pacaur)
			eval "pacaur $pmOptions -Qi $var"
			((rval+=$?))
			;;
			pacman)
			eval "pacman $pmOptions -Qi $var"
			((rval+=$?))
			;;
			apt)
			eval "sudo apt-cache show $var"
			((rval+=$?))
			;;
			rpm)
			# This allows to check a .rpm file for data info
			if [[ -f "$var" ]]; then
				debug "l2" "$var is a file, checking contents for documentation"
				eval "rpm $pmOptions -qip $var"
				((rval+=$?))
			else
				eval "rpm $pmOptions -qi $var"
				((rval+=$?))
			fi
			;;
			yum)
			eval "yum $pmOptions info $var"
			((rval+=$?))
			;;
			emerge)
			eval "equery $pmOptions meta $var"
			((rval+=$?))
			eval "equery $pmOptions depends $var" # Shows dependencies
			((rval+=$?))
			;;
			slackpkg)
			eval "slackpkg $pmOptions info $var"
			((rval+=$?))
			;;
			dnf)
			eval "dnf $pmOptions info $var"
			((rval+=$?))
			;;
			zypper)
			eval "zypper $pmOptions info $var"
			((rval+=$?))
			;;
			pip)
			debug "l3" "Package info lookup not supported yet!"
			return 0
			;;
			*)
			debug "l3" "Unsupported package manager detected! Please contact script maintainer to get yours added to the list!"
			return 1
			;;
		esac
	done
	return "$rval"
}

## checkRequirements()
#
# Function: Check to make sure programs are installed before running script
#
# Call: checkRequirements <program_1> [program_2] [program_3] ...
#
# Input: Each argument should be the proper name of a command or program to be searched for
#
# Output: None if successful, asks to install if anything is found. If it must be manually installed, script will exit.
#
# Other: Except for rare cases, this will not work for libraries ( e.g. anything with "lib" in it). These must be done manually.
#        Note: Now you can use "program/installer" to install program, in case the program is part of a larger package
function checkRequirements() {
	# Determine package manager before doing anything else
	if [[ -z $program || "$program" == "NULL" ]]; then
		determinePM
	fi
	local rval=0
	
	for req in "$@"
	do
		local reqm=""
		local reqt=""
		if [[ "$req" == */* ]]; then
			reqm="$(echo "$req" | cut -d'/' -f1)"
			reqt="$(echo "$req" | cut -d'/' -f2)"
		else
			reqm="$req"
			reqt="$req"
		fi
		
		# No debug messages on success, keeps things silent
		if [[ -z "$(which $reqm 2>/dev/null)" ]]; then
			debug "l1" "$reqt is not installed for $0, notifying user for install"
			getUserAnswer "$reqt is not installed, would you like to do so now?"
			case $? in
				0)
				debug "l1" "Installing $reqt based on user input"
				if [[ -e packageManager.sh ]]; then
					packageManager.sh install "$reqt"
					((rval+=$?))
				elif [[ -e /usr/bin/pm ]]; then
					pm install "$reqt"
					((rval+=$?))
				else
					debug "l4" "Unable to locate packageManager.sh! Please install $reqt manually!"
					exit 1
				fi
				;;
				1)
				debug "l4" "User chose not to install required program $reqt, quitting!"
				announce "Please install the $reqt manually before running this script! Exiting for now..."
				exit 1
				;;
				*)
				debug "l4" "Unknown return option from getUserAnswer: $?"
				announce "Invalid response, quitting"
				exit 1
				;;
			esac
		fi
	done
	
	# If everything is installed, it will reach this point
	debug "l1" "All requirements met, continuing with script!"
	return "$rval"
}

## distUpgradePM()
#
# Function: Upgrade the distribution according to package manager's default instructions (if the feature exists)
#
# Call: distUpgradePM
#
# Input: None
#
# Output: stdout
#
# Other: A warning will be given before proceeding, as this can break systems and dependencies
function distUpgradePM() {
	# Check to make sure $program is set
	if [[ -z $program || "$program" == "NULL" ]]; then
		debug "l2" "Attempted to upgrade package manager without setting program! Fixing..."
		#announce "You are attempting to upgradePM() without setting \$program!" "Script will fix this for you, but please fix your script."
		determinePM
	fi
	local rval=0
	
	debug "l1" "Warning user about running a distribution upgrade"
	announce "WARNING! User indicated to run a distribution upgrade!" "This can be dangerous as it can break system dependencies. Be sure before proceeding."
	pause "Quickly press CTRL+C twice to exit script, or [Enter] to continue"
	case $program in
		apt)
		eval "sudo apt-get $pmOptions dist-upgrade"
		((rval+=$?))
		if [[ ! -z $(which do-release-upgrade 2>/dev/null) ]]; then
			announce "Optionally, you can upgrade to the next release cycle, depending on your settings"
			getUserAnswer "n" "Would you like to upgrade for the latest version of Ubuntu?"
			case $? in
				0)
				debug "l2" "Upgrading Ubuntu system at user request!"
				eval "sudo do-release-upgrade"
				((rval+=$?))
				;;
				*)
				debug "l1" "Not upgrading Ubuntu system"
				;;
			esac
		fi
		;;
		dnf)
		eval "sudo dnf $pmOptions upgrade"
		((rval+=$?))
		;;
		yum)
		eval "sudo yum $pmOptions upgrade"
		((rval+=$?))
		;;
		slackpkg)
		eval "sudo slackpkg $pmOptions install-new" # Required line
		((rval+=$?))
		eval "sudo slackpkg $pmOptions upgrade-all"
		((rval+=$?))
		;;
		zypper)
		eval "sudo zypper $pmOptions update"
		((rval+=$?))
		;;
		rpm)
		eval "sudo rpm $pmOptions -F"
		((rval+=$?))
		;;
		pamac)
		eval "pamac update $pmOptions"
		((rval+=$?))
		;;
		pacman)
		eval "sudo pacman $pmOptions -Syyu"
		((rval+=$?))
		;;
		pacaur)
		eval "pacaur $pmOptions -Syyu"
		((rval+=$?))
		;;
		emerge)
		eval "sudo emerge $pmOptions --changed-use --update --deep @world" # Gentoo is strange
		((rval+=$?))
		;;
		pip)
		pipManage update
		;;
		*)
		debug "l3" "Unsupported package manager detected! Please contact script maintainer to get yours added to the list!"
		return 1
		;;
	esac
   return "$rval"
}

## pipManage()
#
# Function: Manage Python packages using pip3
#           Unlike other functions, all install/remove/update commands handled in one function
#
# Call: pipManage install <package>
#                 remove  <package>
#                 upgrade [package] # Without a package, by default all packages will be updated
#          TODO:  query/search 
#
# Input: Function and package to be operated on
#
# Output: stdout and stderr
#
# Return values: 0 - success
#                1 - failure
#
# Other: Everything managed with pip3. TODO: Code in optional pip support, make /usr/bin/pip point to default
function pipManage() {
   # Verify input
   if [[ -z $1 ]]; then
      debug "l3" "No arguments given to pipManage()! Please check script and re-run!"
      return 1
   elif [[ "$1" == "install" || "$1" == "remove" ]]; then # Make sure second arg present if needed
      if [[ -z $2 ]]; then
         debug "l3" "No package given to pipManage() with $1! Please fix and re-run!"
         return 1
      fi
   fi
   
   # Next, make sure pip is installed
   #if [[ -z $(which pip3 2>/dev/null) ]]; then # This allows for installation outside of /usr/bin
   #   debug "l2" "pip3 not found, attempting to install now!"
   #   if ! universalInstaller "python3-pip"; then
   #      debug "l4" "python3-pip could not be installed! Confirm privileges, install manually, and re-run script!"
   #      return 1
   #   fi
   #else
   #   debug "l5" "pip3 is installed, moving on!"
   #fi
   
   # Set pip run mode
   local rMode="null"
   case "$1" in
      i*|I*)
      rMode="install"
      ;;
      r*|R*|un*|UN*) # remove or uninstall will work here
      rMode="uninstall" # this way I can just run everything in an eval statement
      ;;
      up*|UP*) # Update or upgrade, both so same thing
      rMode="install"
      local updatePip=0 # Tells script to add the update flag
      ;;
      *)
      debug "l3" "Unknown option $1 given! Exiting in failure..."
      return 1
      ;;
   esac
   shift # Now we can cycle through each arg and attempt to $rMode it
   
   # Setup statement to be run by eval
   local statement="sudo -H python3 -m pip $rMode"
   if [[ ! -z $updatePip ]]; then
      statement="$statement --upgrade"
      if [[ -z $1 ]]; then
         debug "l2" "No packages given to pipManage for $rMode! Updating all packages instead..."
         statement="$statement "$(pip3 list 2>/dev/null | cut -d' ' -f1 | tr '\n' ' ') # Very important quote marks are not there, otherwise newlines are copied in as well
      else
         statement="$statement $*"
      fi
   else
      statement="$statement $*"
   fi
   
   # Finally, run the whole thing
   debug "l5" "pipManage() is attempting to eval line: $statement"
   if ! eval "$statement"; then
      debug "l3" "An error occurred while running pip3!"
	  return 1
   else
      debug "INFO: Finished working with pip!"
   fi
   return 0
}

#EOF