#!/usr/bin/env bash
#
# commonVars.sh - A "script" containing default variables for scripts using commonFunctions.sh
#
# NOTE: Script by itself does nothing
#
# If system was setup using myConfig(), some of these might be overwritten. CAPS are "constants"
#
# Changelog:
# v1.0.1
# - Changed default time for getUserAnswer to 60 seconds
# - Updated to user /usr/bin/env bash like the other scripts
# - Also using the -n test parameter now
#
# v1.0.1, 12 Feb. 2021 19:16 PST

### Sanity check
# Check to see if commonVars.sh has been imported yet
# Basically and #IFNDEF to avoid overwriting user-set variables
if [[ -n $cvVar ]]; then
    return
fi

### Globals
# Settings that may be needed by multiple scripts and functions

lpDir="$HOME/.config/linux-pref" # Location where all linux-pref configs and logs will be kept

### Colors
# These are various colors that can be used to change debug and announce output
# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux

BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGREY='\033[0;37m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
NOCOLOR='\033[0m'
NC="$NOCOLOR" # Alias for NOCOLOR

### debug() variables
# These are specifically used for debug(). Explanations below.

debugPrefix="$lpDir/logs"      # Location where debug() sends logfiles
debugInit=0                   # Tells debug() whether or not log setup has been done (this session)
debugLevel=2                  # Default level for debug() output (see debug() in commonFunctions.sh for more info)
debugTimeColor="$GREEN"       # Coloring of relative script time ($SECONDS)
debugNameColor="$YELLOW"      # Color of the script name given by $shortName
debugInfoColor="$WHITE"       # INFO labeled text coloring
debugWarnColor="$YELLOW"      # WARN coloring
debugErrorColor="$LIGHTRED"   # ERROR coloring
debugFatalColor="$RED"        # FATAL coloring
debugDbgColor="$LIGHTPURPLE"  # DBG coloring (level 5)
# NOTE: Above colors were chosen to be similar to dmesg on black background on WSL2 (Ubuntu 18.04)

### announce() variables
# Variables for announce()

announceTimeout=3          # Num of seconds to wait before continuing after outputting an announcement
announceChar="*"           # Default character printed around announce() messages
announceCharColor="$CYAN"  # Color of chars surrounding announcement
announceTextColor="$WHITE" # Color of announcement inside 

### getUserAnswer() variables

getUserAnswerTimeout=60                # Time, in seconds, to wait before moving on in y/n mode
getUserAnswerPromptColor="$LIGHTGREEN" # Color that the "please answer" prompt will be shown in
getUserAnswerAssumeColor="$YELLOW"     # How the (y/n) will be colored in assume mode.  Capital letter will alway be red (for now)

### Other variables

pauseTextColor="$LIGHTCYAN" # Color of text from pause(). Too small a function to need it's own section lol
cvVar=0                     # Tells script to ignore importing if this is set. Avoids accidental overrides.

#EOF