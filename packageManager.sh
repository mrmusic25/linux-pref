#!/usr/bin/env bash
#
# packageManager.sh, a.k.a pm - A universal package manager script
#
# Changes:
# v1.4.1
# - Disabled fwuptool from updateOthers() as it should be part of regular update process
#
# v1.4.0
# - Modernized the script to follow current standards (WIP)
# - Updated test output and help doc
# - Added dist-upgrade functionality
# - Added -f option for forcing a package manager
# - Added firmwareUpdater() to utilize fwupdtool
# - Created support code for new function
# - Re-worded some debug and announce messages
#
# v1.3.2
# - Changed the order commonFunctions.sh is loaded
#
# v1.3.1
# - Updated debug() calls to use commonFunctions.sh v2.0.0
# - Added more verbosity to some debug() calls
#
# v1.3.0
# - Added support for pip through pipManage()
#
# v1.2.13
# - Disabled running rpi-update by default
#
# v1.2.12
# - Gave script its own $aChar, since it is called by many other scripts and I want it to be easily identifiable
#
# v1.2.11
# - Took away sudo, added it to pmCF.sh
# - Now warns user when trying to run as sudo. Can be overridden with -o
# - Side note: I love the fact the last real update to this was almost a year ago lol
# - Script now returns incremental value based on number of errors, coincides with changes to pmCF.sh
#
# v1.2.10
# - Removed some changelog to comply with new rules
#
# v1.2.9
# - Added $shortName and $longName for logging purposes
#
# v1.2.8
# - Fixed an issue preventing program installation from folders from working
#
# v1.2.7
# - After some extensive testing and StackOverflow research, installing from files/directories FINALLY works as planned!
#
# v1.2.6
# - Fixed installation from folder issues incurred from last update
# - Editing text files now edits and deletes a tmp file so main files are not disrupted
# - Added msfupdate to updateOthers()
# - Select commands will now trigger updateOthers(), making it semi-usable!
#
# v1.2.5
# - Switched from a while loop to a for loop, solved the folder installation issues
#
# v1.2.4
# - Fixed adding options (hopefully)
# - no-confirm actually does something now, and should work as well
# - Added a couple of common shorcuts I have become used to using this script the past few weeks (fu, fuc)
# - Added support for updating Raspberry Pi firmware, npm and pip support coming soon
#
# v1.2.3
# - Adding options is broken, attempted a fix, but didn't have time to diagnose it more
#
# v1.2.2
# - Changed the way the script looks for privilege so query and pkginfo trigger the privilege check
#
# v1.2.1
# - Spaces break things. Removed offending spaces.
# - Script will now quit early if not root on most systems
#
# v1.2.0
# - Added option for -n|--no-confirm to reflect new default in pmCF.sh of non-interactive management
# - Gives warnings for dangerous -n cases
#
# v1.1.0
# - Install mode now works with files and folders of files, like programInstaller.sh
#
# v1.0.1
# - Some functions didn't work, fixed it with shift statements
#
# v1.0.0
# - Release version ready
# - Everything is in the processArgs() function
#
# TODO:
#
# v1.4.1, 05 Dec. 2021, 02:40 PST

### Setup

export shortName="pm"
export longName="packageManager"
export aChar="!"

if ! source /usr/share/commonFunctions.sh; then
    if ! source commonFunctions.sh; then
        echo "ERROR: commonFunctions.sh not found! Please install from https://gitlab.com/mrmusic25/linux-pref and re-run script!"
        exit 1
    fi
fi

### Variables

pmOptions="" # Options to be added when running package manager
#runMode="NULL"
program="NULL" # Uninitialized variables are unhappy variables
programMode="name"
confirm=0 # 0 Indicates no change, anything else indicates running noConfirm()
rtnValue=0 # Used to indicate any problems while installing
override=0 # Whether or not to suppress sudo/root error message

### Functions

function displayHelp() {
read -d '' helpVar <<"endHelp"

Usage: pm [options] <mode> [package(s)]
Note: For mode, you can use full names listed below, or single letter in brackets.

Valid package managers: apt, pamac, pacaur, pacman zypper, dnf, yum, slackpkg, emerge, rpm, and pip

Run Modes:
   Re[F]resh                                 : Update the list of available packages and updates from package maintainer
   [U]pgrade                                 : Refresh the package list, and then install all available upgrades
   [I]nstall <package_1> [package_2] ...     : Attempt to install all given packages. Works with text files and directories as well!
   [R]emove  <package_1> [package_2] ...     : Remove given installed packages
   [Q]uery   <package_1> [package_2] ...     : Search package databases for matching package names (does not install)
   [P]kginfo <package_1> [package_2] ...     : Display detailed info (dependencies, version, etc) about packages
   [C]lean                                   : Clean the system of stale and unnecessary packages
   [D]ist-upgrade                            : For distributions that support it, run a distribution upgrade. Runs normal upgrade process for others.
   Firm[W]are                                : Attempts to update firmware using fwupdtool, which is installed if not found.

Options:
   -h | --help                      : Display this help message
   -v | --verbose                   : Display detailed debugging info (note: MUST be first argument!)
   -o | --option <pm_option>        : Any options added here will be added when running the package manager
                                    : Use as many times as needed!
   -r | --override                  : Suppresses error message and allows script to be run as sudo/root
   -f | --force <pacakge_manager>   : Force which package manager to use (see above)
   -n | --no-confirm                : Runs specified actions without prompting user to continue. Not supported by all PMs!
   -p | --pip                       : Tells script to run given commands with python-pip instead of default PM
   
endHelp
echo "$helpVar"
}

function noConfirm() {
	determinePM
	
	case $program in
		apt)
		pmOptions="$pmOptions --assume-yes"
		;;
		pacman|pacaur)
		pmOptions="$pmOptions --no-confirm"
		;;
		dnf|yum)
		pmOptions="$pmOptions ""-y"
		;;
		zypper)
		pmOptions="$pmOptions ""--non-interactive"
		;;
		*)
		# Emerge, slackpkg, rpm do not support assume-yes like commands
		debug "l2" "User specified to run in no-confirm mode, but $program doesn't support it!"
		;;
	esac
}

function processArgs() {
	# Make sure arguments is not empty
	if [[ -z $1 ]]; then
		debug "l3" "$0 requires at least one argument to run!"
		displayHelp
		exit 1
	fi
	
	for var in "$@"
	do
		case "$var" in
			-h|--help)
			displayHelp
			exit 0
			;;
			-o|--option)
			if [[ -z $pmOptions ]]; then
				pmOptions="$2"
			else
				pmOptions="$pmOptions ""$2"
			fi
			shift
			;;
			-r|--override)
			debug "l2" "Enabling override for sudo/root users!"
			override=1
			;;
			-n|--no-confirm)
			confirm=1
			noConfirm
			;;
			-f|--force)
			if [[ -z $2 ]]; then
				debug "l4" "No argument given with $var! Exiting..."
				displayHelp
				exit 1
			else
				debug "l2" "Forcing use of $2 as a package manager!"
				export program="$2"
				shift
			fi
			;;
			-p|--pip)
			debug "l2" "Running the rest of the script with pip!"
			export program="pip" # Suprisingly easy implementation
			;;
			fu|FU)
			updatePM
			((rtnValue+=$?))
			upgradePM
			((rtnValue+=$?))
			updateOthers
			;;
			fuc|FUC)
			updatePM
			((rtnValue+=$?))
			upgradePM
			((rtnValue+=$?))
			updateOthers
			if [[ $confirm -ne 0 ]]; then
				debug "l1" "Warning user against cleaning the package manager non-interactively..."
				announce "WARNING: It can be dangerous to clean package managers without confirmation!" "Script will continue shortly, but it is recommended to CTRL+C now!"
				sleep 5
			fi
			cleanPM
			((rtnValue+=$?))
			;;
			f|F|refresh|Refresh|update|Update) # Such alias.
			updatePM
			((rtnValue+=$?))
			;;
			u|U|upgrade|Upgrade)
			upgradePM
			((rtnValue+=$?))
			updateOthers
			;;
			c|C|clean|Clean)
			# Give a warning if running in non-interactive mode
			if [[ $confirm -ne 0 ]]; then
				debug "l1" "Warning user against cleaning the package manager non-interactively..."
				announce "WARNING: It can be dangerous to clean package managers without confirmation!" "Script will continue shortly, but it is recommended to CTRL+C now!"
				sleep 5
			fi 
			cleanPM
			((rtnValue+=$?))
			;;
			d|D|dist-upgrade|distribution-upgrade|distupgrade)
			if [[ $confirm -ne 0 ]]; then
				debug "l2" "User is attempting to run a dist-upgrade in no-confirm mode, warning them against dangers!"
				announce "WARNING: It is not recommended to perform a distribution upgrade with confirmation off!" "Script will continue shortly, but press CTRL+C now to stop it!"
				sleep 5
			fi
			distUpgradePM
			((rtnValue+=$?))
			;;
			i|I|install|Install)
			shift
			for prog in "$@"
			do
				programInstaller "$1"
				shift
			done
			;;
			r|R|remove|Remove)
			shift
			# Give a warning if running in non-interactive mode
			if [[ $confirm -ne 0 ]]; then
				debug "l1" "Warning user against removing programs non-interactively..."
				announce "WARNING: It is dangerous to remove programs without confirmation!" "Script will continue shortly, but it is highly recommended to CTRL+C now!"
				sleep 5
			fi
			
			for prog in "$@"
			do
				removePM "$1"
				((rtnValue+=$?))
				shift
			done
			;;
			q|Q|query|Query)
			shift
			for prog in "$@"
			do
				queryPM "$1"
				((rtnValue+=$?))
				shift
			done
			;;
			p|P|pkg*|Pkg*) # Hopefully this works the way I want, wild cards can be tricky
			shift
			for prog in "$@"
			do
				pkgInfo "$1"
				((rtnValue+=$?))
				shift
			done
			;;
			firmware|Firmware|w|W)
			if [[ $confirm -ne 0 ]]; then
				debug "l4" "Denying user attempt to update firmware in no-confirm mode!"
				announce "ERROR: Cannot update firmware in no-confirm mode!" "Please run manually using fwuptool, or re-run script without no-confirm flag!" "Exiting for now..."
				displayHelp
				exit 1
			else
				debug "l1" "Attempting to update firmware at user request!"
				if ! firmwareUpdater; then
					debug "l2" "An error occurred while running firmwareUpdater(), see log for details!"
				else
					debug "l1" "Successfully ran fwupdtool"
					if getUserAnswer "It is highly recommended to reboot the system after a firmware upgrade, would you like to do so now?"; then
						debug "l1" "System is going to reboot now after updating firmware!"
						sudo reboot
						exit 0
					else
						debug "l2" "User denied rebooting, continuing!"
					fi
				fi
			fi
			;;
		esac
		shift
	done
}

function programInstaller() {
	# Decide on mode
	if [[ -f $1 ]]; then
		debug "l1" "$1 is a file, running in file mode!"
		local file=$1
		local programMode="file"
	elif [[ -d $1 ]]; then
		debug "l1" "$1 is a folder, running in directory mode!"
		local file=$1
		local programMode="directory"
	else
		debug "l5" "Argument is not a file or folder, assuming $1 is a program!"
	fi
	local rtnValue=0
	# Now, install based on mode. Taken from the old programInstaller.sh
	case $programMode in
		file)
		announce "Now installing programs listed in $file!" "This may take a while depending on number of updates and internet speed" "Check $logFile for details"
		getUserAnswer "Would you like to edit $file before installing?"
		case $? in
			0)
			fileTMP="$file".tmp # Temp file, so main files are not edited
			cp "$file" "$fileTMP"
			editTextFile "$fileTMP"
			;;
			1)
			fileTMP="$file".tmp # Temp file, so main files are not edited
			cp "$file" "$fileTMP"
			debug "l1" "User chose not to edit $file"
			;;
			*)
			debug "l3" "Received an unknown return value from getUserAnswer()! Attempting to continue..."
			((rtnValue++))
			;;
		esac
		
		#OIFS=$IFS
		#IFS=$'\n' # Change to IFS is necessary
		#set -f # Disables globbing
		while read -r line; # Not sure why the community dislikes cat, but it works
		do
			if [[ $line != \#* ]]; then      # Skips comment lines
				universalInstaller "$line"
				((rtnValue+=$?))
			fi
		done < "$fileTMP"
		#IFS=$OIFS # Reset IFS and globbing so the rest of the script doesn't break
		#set +f
		
		rm "$fileTMP"
		;;
		directory)
		announce "Installing programs from files in directory $file!" "This WILL take a long time!" "Don't go anywhere, you will be asked if each section should be installed!"
		cd "$file"
		for list in *.txt;
		do
			debug "l1" "Asking user if they would like to install $list"
			getUserAnswer "Would you like to install the programs listed in $list?"
			if [[ $? -eq 1 ]]; then
				debug "l1" "Skipping $list at user's choice..."
			else
				# Ask if user wants to edit file before installing
				getUserAnswer "Would you like to edit $list before installing?"
				case $? in
				0)
				listTMP="$list".tmp # Temp file, so main files are not edited
				cp "$list" "$listTMP"
				editTextFile "$listTMP"
				;;
				1)
				listTMP="$list".tmp
				cp "$list" "$listTMP"
				debug "l1" "User chose not to edit $list"
				;;
				*)
				debug "l3" "Received an unknown return value from getUserAnswer()! Attempting to continue..."
				((rtnValue++))
				;;
				esac
				
				#OIFS=$IFS
				#IFS=$'\n' # Change to IFS is necessary
				#set -f # Disables globbing
				while read -r line; # Not sure why the community dislikes cat, but it works
				do
					if [[ $line != \#* ]]; then      # Skips comment lines
						universalInstaller "$line"
						((rtnValue+=$?))
					fi
				done < "$listTMP"
				#IFS=$OIFS # Reset IFS and globbing so the rest of the script doesn't break
				#set +f
				
				rm "$listTMP" # Leave no trace. Besides what the user wants, that is
			fi
		done
		[[ ! -z $OLDPWD ]] && cd "$OLDPWD" # Return to previous location so other scripts don't break
		;;
		name)
		universalInstaller "$1"
		((rtnValue+=$?))
		;;
		*)
		debug "l4" "Everything is broken. Why. At least you have unique debug messages for an easy CTRL+F."
		exit 1
		;;
	esac

	return $rtnValue
}

function updateOthers() {
	# Check to see if there are other installers/updaters, and offer to update them
	
	# rpi-update - Firmware updater for Raspberry Pi distros
	if [[ ! -z $(which rpi-update 2>/dev/null) ]]; then
		getUserAnswer "n" "Raspberry Pi detected, would you like to update firmware with rpi-update?"
		case $? in
			0)
			eval "sudo rpi-update"
			((rtnValue+=$?))
			;;
			1)
			false
			;;
			*)
			debug "l3" "Unknown value from getUserAnswer()!"
			;;
		esac
	fi
	
	# Update Metasploit framework using msfupdate
	if [[ ! -z $(which msfupdate 2>/dev/null) ]]; then
		getUserAnswer "n" "Metasploit detected, would you like to update as well?"
		case $? in
			0)
			eval "sudo msfupdate"
			((rtnValue+=$?))
			;;
			1)
			false
			;;
			*)
			debug "l3" "Unknown value from getUserAnswer()!"
			;;
		esac
	fi

	# fwupdtool - Updated system firmware in Linux
	#if [[ -n $(which fwupdtool 2>/dev/null) ]]; then
	#	if getUserAnswer "y" "fwupdtool was found, would you like to attempt to install new firmware?"; then
	#		firmwareUpdater
	#	fi
	#fi
	return 0	
}

function firmwareUpdater() {
	if [[ -z $(which fwuptool 2>/dev/null) ]]; then
		debug "l2" "fwupdtool is not installed! Installing now..."
		if ! universalInstaller "fwupd"; then
			debug "l3" "Could not install fwupd tool! Abandoning firmware upgrade, please install manually!"
			return 1
		fi
	fi

	debug "l1" "Refreshing firmware database..."
	if ! eval "sudo fwuptool refresh"; then
		debug "l3" "Could no refresh database using fwupdtool! Please check internet connection and try again!"
		return 1
	fi

	debug "l1" "Attempting to install any new firmware available!"
	if ! eval "sudo fwupdtool upgrade"; then
		debug "l3" "fwupdtool encountered an error and could not install firmware! Exit code: $?..."
		return 1
	fi
	return 0
}
### Main Script

# Warn users against running as root
if [[ "$EUID" -eq 0 && "$override" -eq 0 ]]; then
	debug "l1" "Script was run as root without override, warning user"
	announce "ERROR: Please do not run $longName as sudo/root!" "Script will use sudo when necessary automatically" "You can override this with the -r|--override option. Exiting for now."
	exit 1 # That way [ -gt $x ] and [ -ne 0 ] work
fi

determinePM

processArgs "$@" # Didn't plan it this way, but awesome that everything work with this script

if [[ $rtnValue -ne 0 ]]; then
	debug "l2" "Finished script with some errors, see log for details! Exit-code: $rtnValue"
else
	debug "l1" "Script has completed successfully!"
fi
exit "$rtnValue"

#EOF