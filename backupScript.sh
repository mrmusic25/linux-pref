#!/usr/bin/env bash
#
# backupScript.sh - A script to backup files and folders via a CSV list
#
# Usage: ./backupScript.sh [options]
#
# Changelog:
# v0.5.0
# - Added priorityBackup() and supporting code
# - Removed selfBackup and references since use case changed
# - Included more checks in processArgs()
# - Added checks for output drive storage space availability
# - Fixed almost all of the shellcheck issues
#
# v0.4.0
# - Finished coding all the options
# - Wrote the ridiculously short main() section
# - Added exportConfig() for writing to myConfig, as well as supporting code
# - I think it's ready for testing and release!
#
# v0.3.0
# - Added force option for processing to always make an item go through (useful for self-backup)
# - Added dedupFolder() to remove old backups
# - Put in checks to make sure Permananent/ folders are not touched
# - Added argument and supporting code for backing up a single line in the database
# - Also added support for manual backups with the same argument
#
# v0.2.0
# - Added backupItem()
# - Added processDatabase()
# - Added options for changing permissions and ownership of output folders
# - Almost ready for release! Just a couple more functions, and testing!
#
# v0.1.1
# - Wrote addLine()
# - Added custom config location for myConfig()
# - Also option to make config global via /usr/share
#
# v0.1.0
# - Erased all the old work from 2019 (wow)
# - Got the help section ready, now I just need to code around it
# - Almost done getting processArgs() ready with all the options
# - Now focusing on backing up files and folders with compression
#
# v0.0.1
# - Initial version
# - Initially wanted to have everything back up in a before-main-after system over Samba
# - Too ambitious, which is why it took two years to write this
#
# TODO:
# - Make everything use myConfig()
# - Copy to smb/nfs share
# - Hook so when external drive is plugged in, automatically move latest backups to drive
#   ~ Base it off UUID of drive/part
#   ~ Maybe make it a flag in the CSV? Or it could be a timeMode
# - Make -e|--edit more of an interactive editor
# - Allow user to specify conpression level, since they all use a scale of 1-9
# - Allow changing of chmod (currently 0640 for privacy)
# - Make function to copy the latest single backup of each type to an external drive
#
# v0.5.0, 08 Feb. 2021 22:11 PST

### Script setup

export longName="backupScript"
export shortName="bs"
if ! source /usr/share/commonFunctions.sh; then
    if ! source commonFunctions.sh; then
        echo "ERROR: commonFunctions.sh not found! Please install from https://gitlab.com/mrmusic25/linux-pref and re-run script!" 1>&2
        exit 1
    fi
fi

### Variables

database="$HOME/.backupScriptDB.csv" # Location of CSV used for database
outputFolder="$HOME/Backups"         # Folder where backups will be stored
compMode="tar.gz"                    # Type of compression (if any) to use for backups
dateMode="%d-%b-%y_%H-%m-%S"         # Format of the GNU/date command to append to backups
runTime="none"                       # What unit of time the script will operate in (hourly, weekly, etc). See below for valid uses.
numBackups=10                        # Number of backups to keep before deletion
doManifest=0                         # Whether or not to generate a sha512 manifest of folder before compressing
saveConfig=1                         # Tells script to save any changes to the config
doGlobal=1                           # Flag to indicate usage of global config in /usr/share
defaultPriority=100                  # Priority to use when none is given
setOwner="$USER:$USER"               # User:Group ownership of the outputFolder will be changed to each run
chmodLevel="0640"                    # Permissions level of the whole outputFolder will be changed to after each run
spacePercent=10                      # Percentage delta of how much space is "needed" on drive to run backup

# Set the location of the myConfig() .conf file
# Prefer existing config (user, then global), otherwise make new user config
if [[ -f "$lpDir/.backupScript.sh" ]]; then
    myConfigLocation="$lpDir/.backupScript.conf"
elif [[ -f "/usr/share/backupScript.conf" ]]; then
    myConfigLocation="/usr/share/backupScript.conf"
elif [[ -n $lpDir ]]; then
    myConfigLocation="$lpDir/.backupScript.conf"
else
    myConfigLocation="$HOME/.backupScript.conf"
fi

### Functions

function displayHelp() {
read -d '' helpVar <<"endHelp"

Usage: ./backupScript.sh [options]

Options:
   -h | --help                                : Display this help message and exit
   -v | --verbose                             : Enables verbose logging. Use -vv | --vverbose for extra debug info.
   -c | --config <file>                       : Moves the config for this file to /usr/share for global usage
   -g | --global <on/off>                     : Enable/disable global mode for the given myConfig() file
   -l | --list [database.csv]                 : List all the saved items and their settings, then exits. Useful when making a permanenet backup (below)
   -a | --add <time> <item> [priority]        : Add new file or folder to backup, and optionally it's priority. See below for more info. Can be used multiple times.
   -f | --folder <output_folder>              : Sets the output folder and saves it as the default
   -o | --output <folder> [default]           : Changes the output folder for this run. Makes it the default if arguments is present.
   -z | --compression <mode> [default]        : Specifies which compression method to use. If 'default' is present, it will save method as the default.
   -e | --edit [database.csv]                 : Lets user edit database, or given one, in $EDITOR (nano if unset) then exits
   -t | --time <time>                         : Sets the time mode for backing up the database (see below)
   -n | --num-backups <number>                : Sets the number of backups to keep of each item name
   -m | --manifest <on/off> [default]         : Disables generation of a manifest file generated with sha256sum
   -p | --permanent [num]                     : Sends this backup to the Permanent/ folder, runs line num if present
   -d | --date-format <format> [default]      : Sets the format for GNU/date (default is '+%d-%b-%y_%H-%m-%S', or '21-Jan-21_22-01-03' for Jan. 21, 2021 at 10:01:03)
   -u | --user <user | user:group>            : User that ownership of the backup folder will be set to after each run. UID acceptable. If only User given, it will use it for group as well.
   -r | --permissions <level>                 : Permissions level to change output_folder to. Uses chmod so any value that works there works here (default is 0640).

Item:
   An item can be either a folder or a directory
   Single items will be backed up as is, whereas folders will be compressed (if enabled)

Time:
   Use one of the following options whenever <time> is needed:

   hourly - Items backed up every hour (network access logs, Minecraft server files)
   daily - Items backed up every day (current projects, homework)
   weekly - Items to be backed up once a week (config files)
   monthly - Items backed up once a month (whole documents folder)
   all - Backs up all stored items regardless of time (runs by default if no arguments)

   NOTE: These are just recommendations for use. Definitely use a large hard drive, because even compressed these backups will take up a lot of space!
   By default, 10 backups of each item will be kept before the oldest is deleted.

Output folder:
   This is how the directory scructure of <output_folder> will look:

   ./<output_folder>/
      ~ daily/ # Folders will not be created unless they are used, hence no "hourly/" folder
         - var-log.d/   # Each item is given its own folder. For example, here is /var/log
           * backup-DATE.7z 
           * backup-DATE1.7z
         - linux-pref.d/
           * backup-DATE(.7z)   # If compression is disabled, it will just make a copy of the item here with the date appended instead
              + programLists/   # Subfolders will be included
                > xrdp.txt
                  .  .  .
              + backupScript.sh
              + commonFunctions.sh
                 .  .  .
              + manifest.txt # Contains sha512sum and full filename
      ~ weekly/
         - etc-fstab.d/
           * backup-DATE   # Single files, such as /etc/fstab, will not be compressed (may add option later)
           * backup-DATE1  
           * manifest.txt  # A manifest will still be generated and added to for single files (if enabled)
      ~ permanent/     # Anything moved to the Permanent folder will not be touched
         - etc-fstab/  # Because of this, these should only be done as a one-shot. Old backups can be useful, like an initial config.
            * backup-DATE 

Compression:
   The following modes of compression are supported by this script (in order of preference):
   NOTE: Start at the highest compression but most time to complete at the top, then vice versa at the bottom

   7zip   - Compressed using lzma2 and max settings. Very RAM and CPU intensive, but smallest archive size so it's worth it. Needs installation usually.
   tar.xz - A more compressed version of tar.gz. Installation might be needed but included in most distros nowadays.
   tar.gz - The de-facto archive format of Linux, haven't met a distro so far that doesn't have built-in support.
   zip    - Good if you need easy Windows access, but almost always needs to be installed first. OK compression ratio.
   tar    - Uncompressed, simply archived for easy data transfer
   none   - Makes a direct copy instead of archiving and compressing

   Script will select the first one found installed by default on first run.
   When user manually changes format, it will be installed (if needed)

Priority:
   The priority of an item indicates the order in which they will be copied to an external drive, in case said drive runs out of room.
   Zero (0) is the highest priority; as x increases, the priority decreases.
   The default priority of an item is 100.

Misc:
   When using -p|--permanent, it is recommended to use -l|--list first to get the number of the item you want to make a permanent backup of.
      Otherwise, all items in database will be given a single permanent backup
      This does have its uses, but can be time and resource consuming, especially for a one-off backup.

endHelp
echo "$helpVar"
}

function processArgs() {
    if [[ -z $1 ]]; then
        debug "l2" "No arguments, attempting to run without user input!"
        return 0
    fi
    
    while [[ -n $1 ]]; do {
        debug "l0" "pA(): Processing key $1"
        
        case "$1" in
        -h|--help)
        debug "l1" "Displaying help and exiting!"
        displayHelp
        exit 0
        ;;
        -v|--verbose)
        export debugLevel=1
        debug "l1" "Verbose logging enabled"
        ;;
        -vv|--vverbose)
        export debugLevel=0
        debug "l0" "Debug logging enabled"
        ;;
        -f|--folder)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No folder given with $1! Please fix and re-run!"
            return 1
        elif [[ "$2" != /* ]]; then
            debug "l2" "Given argument $2 is not a full directory, assuming current directory!"
            outputFolder="$(pwd)/$2"
        else
            debug "l1" "Setting outputFolder to $2"
            outputFolder="$2"
            saveConfig=0
        fi
        shift
        ;;
        -u|--user)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No option given with $1! Please fix and re-run!"
            return 1
        elif [[ "$2" != *:* ]]; then
            debug "l2" "Only user $2 was specified, using it as group as well!"
            setOwner="$2:$2"
        else
            debug "l1" "Using $2 as user/group for backups!"
            setOwner="$2"
        fi
        shift
        ;;
        -r|--permission*)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No level given to $1! Please fix and re-run!"
            return 1
        else
            debug "l2" "Using $2 as a permission level, not checking validity!"
        fi
        chmodLevel="$2"
        shift
        ;;
        -z|--compression)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No option given with $1! Please fix and re-run!"
            return 1
        fi
        case "$2" in # Easier than doing a big if statement tbh
            7zip|tar.gz|tar.xz|zip|tar|none)
            debug "l1" "Setting compression mode to $2"
            compMode="$2"
            if [[ -z $3 && "$3" != -* ]]; then
                debug "l0" "Saving config after setting compression mode 3=$3"
                saveConfig=0
                shift
            fi
            ;;
            *)
            debug "l4" "Unknown mode $2 given with $1! Please fix and re-run!"
            return 1
            ;;
        esac
        shift
        ;;
        -d|--date-format)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No option given with $1! Please fix and re-run!"
            return 1
        fi
        if [[ "$2" != *+%* ]]; then
            debug "l3" "Given date format $2 is not valid! Not changing, attempting to continue..."
        elif ! date "$2" 1>/dev/null 2>/dev/null; then
            debug "l3" "Date rejected format $2! Please consult man pages and retry! Attempting to continue..."
        else
            debug "l1" "Changing date format to $2"
            dateMode="$2"
            if [[ -z $3 && "$3" != -* ]]; then
                debug "l0" "Saving config after setting date format 3=$3"
                saveConfig=0
                shift
            fi
        fi
        shift
        ;;
        -m|--manifest)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No option given with $1! Please fix and re-run!"
            return 1
        fi
        if yesorno "$2"; then
            debug "l1" "Enabling archive manifests"
            doManifest=0
        else
            debug "l1" "Disabling archive manifests"
            doManifest=1
        fi
        if [[ -z $3 && "$3" != -* ]]; then
            debug "l0" "Saving config after setting manifest 3=$3"
            saveConfig=0
            shift
        fi
        shift
        ;;
        -n|--num-backups)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No option given with $1! Please fix and re-run!"
            return 1
        elif [[ $2 -ne $2 ]]; then
            debug "l3" "Given argument $2 is not a number! Not changing numBackups from $numBackups, attmepting to continue..."
        else
            debug "l2" "Changing numBackups from $numBackups to $2 permanently!"
            numBackups="$2"
            saveConfig=0
        fi
        shift
        ;;
        -c|--config)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No config file given with $1! Please fix and re-run!"
            return 1
        fi
        debug "l2" "Setting config file to $2"
        myConfigLocation="$2"
        if [[ "$myConfigLocation" != /* ]]; then
            debug "l2" "$myConfigLocation is not a full path, assuming current directory..."
            myConfigLocation="$(pwd)/$myConfigLocation"
        fi
        if [[ -f "$2" ]]; then
            debug "l2" "Config file at $2 exists, importing! Other variables will be overwritten!"
            myConfig "$myConfigLocation" "import"
        fi
        shift
        ;;
        -g|--global)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No bool option given with $1! Please fix and re-run!"
            return 1
        fi

        if yesorno "$2"; then
            debug "l2" "Forcing usage of default global config location!"
            myConfigLocation="/usr/share/backupScript.conf"
            if [[ -f "$myConfigLocation" ]]; then
                debug "l2" "Global config at $myConfigLocation exists, importing! Variables will be overwritten!"
                myConfig "$myConfigLocation" "import"
            fi
        fi
        shift
        ;;
        -a|--add)
        local lloc
        local ltime
        local lpri

        # First make sure item exists and is full path
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "Missing arguments from $1! Please fix and re-run!"
            return 1
        elif [[ "$3" != /* ]]; then
            debug "l2" "Given argument $3 is not a full path, assuming current directory..."
            lloc="$(pwd)/$3"
        else
            lloc="$3"
        fi

        if [[ ! -e "$lloc" ]]; then
            debug "l4" "Given item $lloc does not exist! Please verify and try again!"
            return 1
        else
            debug "l0" "Location ($lloc) given to $1 is valid!"
        fi

        # Next, verify time is valid
        ltime="$(echo "$2" | awk '{print tolower($0)}')"
        case "$ltime" in
            hourly|daily|weekly|monthly)
            debug "l0" "$ltime is a valid unit of time"
            ;;
            *)
            debug "l4" "Given argument $ltime is not a valid unit of time! Please fix and re-try!"
            return 1
            ;;
        esac

        # Finally, set priority
        if [[ -z "$4" || "$4" -ne "$4" ]]; then
            debug "l2" "No priority given to $lloc, assuming default of $defaultPriority..."
            lpri="$defaultPriority"
        else
            debug "l0" "Using given priority of $4 for $lloc"
            shift
        fi

        # Attempt to add the file and report the result
        if ! addLine "$ltime" "$lloc" "$lpri"; then
            debug "l4" "A problem occurred while adding $lloc, please fix and re-try!"
            return 1
        fi # Else, successful. No reason to report since function already does.
        shift
        shift
        ;;
        -e|--edit)
        local lfile
        if [[ -n $2 && "$2" != -* && -f "$2" ]]; then
            debug "l1" "Selecting user given database $2"
            lfile="$2"
            shift
        else
            debug "l0" "Using already set database $database for editing"
            lfile="$database"
        fi
        
        local prog
        if [[ -n $EDITOR ]]; then
            prog=$EDITOR
        elif [[ -n $VISUAL ]]; then
            prog=$VISUAL
        elif ! which nano 1>/dev/null; then
            prog="vi"
        else
            prog="nano"
        fi

        debug "l1" "Letting used edit $lfile with $prog"
        if ! eval "$prog $lfile"; then
            debug "l4" "Unable to edit $prog! Does $USER have permission? Please fix and re-run!"
            return 1
        else
            debug "l1" "Done editing, attmepting to continue..."
        fi
        ;;
        -l|--list)
        local ffile
        local fnum=1
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l2" "No file given with $1, assuming default!"
            ffile="$database"
        fi

        if [[ ! -f "$ffile" ]]; then
            debug "l4" "Database at $ffile does not exits, or is not readable! Please fix and re-run!"
            return 1
        fi

        debug "l1" "Listing contents of $ffile and exiting..."
        while read -r line; do
            if [[ "$line" != \#* ]]; then
                printf "%s,%s\n" "$fnum" "$line" | column -t -s,
            fi
            ((fnum++))
        done
        exit 0
        ;;
        -p|--permanent)
        runTime="permanent"
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l2" "No line given to backup, backup up whole database one time to Permanent folder!"
        elif [[ $2 -ne $2 ]]; then
            debug "l4" "Argument given with $1 is not a number! Please fix and re-run!"
            return 1
        else
            debug "l1" "Attempting to run single database entry # $2!"
            if ! processDatabase "$2"; then
                return 1
            else
                exit 0
            fi
        fi
        ;;
        -t|--time)
        if [[ -z $2 || "$2" == -* ]]; then
            debug "l4" "No time mode given with $0! Please fix and re-run!"
            return 1
        fi
        local xmode="$(echo "$2" | awk 'print tolower($0)')"
        case "$xmode" in
            hourly|daily|weekly|monthly|manual|permanent)
            debug "l1" "Setting run mode to $2!" 
            ;;
            *)
            debug "l4" "Time mode $xmode is invalid! Please fix and re-run!"
            return 1
            ;;
        esac
        runTime="$xmode"
        shift
        ;;
        -o|--output)
        if [[ -z "$2" || "$2" == -* ]]; then
            debug "l4" "No folder given with $1! Please fix and re-run!"
            return 1
        fi
        debug "l1" "Setting output folder to $2"
        outputFolder="$2"

        if [[ -n "$3" && "$3" != -* ]]; then
            debug "l2" "Making output folder $outputFolder permanent!"
            saveConfig=0
            shift
        fi
        shift
        ;;
        *)
        debug "l2" "Unknown option $1 given! Please fix and re-run!"
        return 1
        ;;
        esac
        shift
    } done # This should be sufficient, since there's no required arguments to run the script
    
    if [[ "$saveConfig" -eq 0 ]]; then
        debug "l2" "Saving config with new values!"
        exportConfig
    fi
    return 0
}

# Usage: addLine <time> <item> [priority]
function addLine() {
    # Make sure everything is valid
    if [[ -z $1 ]]; then
        debug "l4" "No arguments given to addLine()! Please fix and re-run!"
        return 1
    elif [[ $# -lt 2 ]]; then
        debug "l4" "Too few arguments given to addLine()! Please fix and re-run!"
        return 1
    else
        local ltime="$1"
        local lloc="$2"
        local lpri="$3"
        local lstring=" "
        if [[ -z $lpri ]]; then
            debug "l0" "addLine() given args time=$ltime location=$lloc, no priority so using default=$defaultPriority"
            lstring="$(printf "%s,%s,%s\n" "$ltime" "$lloc" "$defaultPriority")"
        else
            debug "l0" "addLine() given args time=$ltime location=$lloc priority=$lpri"
            lstring="$(printf "%s,%s,%s\n" "$ltime" "$lloc" "$lpri")"
        fi
    fi

    # Add comment to the CSV if it's the first push
    if [[ ! -f "$database" ]]; then
        debug "l1" "Creating new database $database!"
        if ! printf '# time, location, priority\n' > "$database"; then
            debug "l4" "Could not write to $database, does $USER have permission? Please fix and re-run!"
            return 1
        fi
    fi

    debug "l0" "Appending line $lstring to $database"
    if ! echo "$lstring" | tee -a "$database" 1>/dev/null; then
        debug "l4" "Could not add line to $database, does $USER have permission? Please fix and re-run!"
        return 1
    fi

    debug "l1" "Added folder $lloc to $ltime database with priority $lpri!"
    return 0
}

# Processes each line in the database
# If $1 is a number, script will attempt to do just that line with the "manual" option
function processDatabase() {
    if [[ ! -f "$database" ]]; then
        debug "l4" "Cannot read given database $database, does $USER have permission? Please fix and re-run!"
        return 1
    fi

    local dmode=0
    if [[ -n $1 ]]; then
        dmode="$1"
        if [[ $dmode -ne $dmode ]]; then
            debug "l4" "Argument $1 given to processDatabase() is not a number! Please fix and re-try!"
            return 1
        else
            debug "l2" "processDatabase will only process line $1!"
        fi
    fi

    local dtime      # Time to be run
    local dloc       # Location of the item
    #local dname      # The basename of the item (no preceeding folders or slashes)
    local dout       # Output directory of the item
    local ddate      # Time when this job was started to append to item
    #local ddir       # Directory of the first item without the basename (for recreating subdirectories)
    local dnum=1     # Line number for user reference during errors

    while read -r line; do {
        ## Example in comments based on line: "monthly,/mnt/Data/Documents,5"
        # Skip comments so numbers match up with -o
        if [[ "$line" == \#* ]]; then
            continue
        fi

        # Make sure time is valid
        ddate="$(date "+$dateMode")"
        if [[ "$runTime" == "permanent" ]]; then
            dtime="$runTime" # This way, permanent runs get stored in the Permanent/ folder
        else
            dtime="$(echo "$line" | cut -d',' -f1 | awk 'print tolower($0)')" ## monthly
        fi

        case "$dtime" in
            hourly|daily|weekly|monthly|manual)
            true
            ;;
            *)
            debug "l3" "Invalid unit of time ($dtime) given in line $dnum: $line. Skipping line..."
            ((dnum++))
            continue
            ;;
        esac

        # Set location, basename ($dname), and output location ($dout)
        dloc="$(echo "$line" | cut -d',' -f2 | sed -e 's/"//g')" ## dloc = /mnt/Data/Documents/
        if [[ "$dloc" == */ ]]; then   # Remove trailing / if it has one, to prevent wrong cut for dname
            dloc="${dloc%?}" ## dloc = /mnt/Data/Documents
        fi
        #dname="$(echo "$dloc" | rev | cut -d'/' -f 1 | rev)" ## dname = Documents
        #ddir="$(echo "$dloc" | rev | cut -d '/' -f 1 --complement | rev)" ## ddir = /mnt/Data

        dout="$outputFolder$dloc-${dtime^}-$ddate" # Extension will be handled by backupItem()

        # No need for priority here, only used in other functions
        
        if [[ $dmode -eq 0 ]] && [[ "$dtime" == "$runTime" || "$runTime" == "manual" || "$runTime" == "permanent" ]]; then
            debug "l0" "Running backup; dloc=$dloc, dout=$dout"
            if ! backupItem "$dloc" "$dout"; then
                debug "l3" "A problem occurred while backing up $dloc! Attempting to continue..."
            else
                debug "l1" "Done backing up $dloc!"
            fi
        elif [[ $dmode -ne 0 ]]; then
            if [[ $dnum -eq $dmode ]]; then
                debug "l0" "Running singluar backup; dloc=$dloc, dout=$dout"
                if ! backupItem "$dloc" "$dout"; then
                    debug "l2" "An error occurred while processing $dloc, see log for more info!"
                    return 1
                else
                    debug "l1" "Successfully processed $dloc, exiting now!"
                    return 0
                fi
            fi
        fi # else, item isn't supposed to run
        ((dnum++))  
    } done < "$database"

    # Cleanup
    # Change ownership and permissions of entire folder
    if ! chmod -R "$chmodLevel" "$outputFolder"; then
        debug "l2" "Could not change permissions of $outputFolder! Please do so manually!"
    elif ! chown -R "$setOwner" "$outputFolder"; then # No reason to run this if previous command failed
        debug "l2" "Could not change ownership of $outputFolder to $setOwner! Please do so manually!"
    else
        debug "l1" "Changed ownership and permissions of $outputFolder to $setOwner and $chmodLevel!"
    fi

    return 0
}

# Usage: backupItem <input_item> <output_folder>
function backupItem() {
    # Sanity check on required arguments
    if [[ -z $1 ]]; then
        debug "l3" "No arguements given to backupItem()! Returning..."
        return 1
    elif [[ ! -e "$1" ]]; then
        debug "l3" "Item at $1 does not exits, unable to backup! Returning..."
        return 1
    elif [[ -z $2 ]]; then
        debug "l3" "Backup folder for $1 not given, cannot continue! Returning..."
        return 1
    else
        local qin="$1"
        local qout="$2"
        local qfol="$(echo "$qout" | rev | cut -d'/' -f1 --complement | rev)"
        local qman="$qin/manafest.txt"
    fi

    # Make sure parent directories can be created
    if ! mkdir --parents "$qfol"; then
        debug "l3" "Unable to create parent directories $qfol! Does $USER have permission? Returning..."
        return 1
    fi

    # Clean folder of old backups before continuing
    dedupFolder "$qfol"

    # Make sure there is sufficient space before compressing
    # Calculation is using a delta, so setting $spacePercent to 10 will use a calculation of 90%
    local inSize="$(du -bs "$qin" | cut -d' ' -f1)"
    local outSize="$(df --output=avail "$qout" | grep -V avail)"
    if [[ $((inSize*spacePercent/100)) -gt $outSize ]]; then
        debug "l3" "Not enough space on $qout to compress $qin: input $inSize vs output $outSize, spacePercent=$spacePercent"
        return 1
    fi

    # Make the manifest file
    if [[ -f "$qin" ]]; then
        debug "l1" "Item $qin is a file, not creating a manifest!"
    elif [[ $doManifest -eq 0 ]]; then
        debug "l1" "Creating a manifest of $qin at $qman!"
        indexFolder "$qin" "$qman" "hash" # TODO: Turn sha256 sum off in config
    else
        debug "l0" "Skipping creating a manifest for $qin"
    fi

    # Now, backup based on $compMode
    debug "l1" "Beginning backup of $qin to $qout"
    case "$compMode" in
        7z)
        qout="$qout.7z"
        if ! 7z a -t7z "$qout" -m0=lzma2 -mx=9 "$qin"; then
            debug "l3" "A problem ocurred running 7zip! Return code: $?"
            return 1
        else
            debug "l1" "Successfully compressed $qout!"
        fi
        ;;
        tar.gz)
        qout="$qout.tar.gz"
        if ! env GZIP=-9 tar czf "$qout" "$qin"; then
            debug "l3" "A problem ocurred running tar.gz! Return code: $?"
            return 1
        else
            debug "l1" "Successfully compressed $qout!"
        fi
        ;;
        tar.xz)
        qout="$qout.tar.xz"
        if ! env XZ_OPT=-9 tar cJf "$qout" "$qin"; then
            debug "l3" "A problem ocurred running tar.xz! Return code: $?"
            return 1
        else
            debug "l1" "Successfully compressed $qout!"
        fi
        ;;
        tar)
        qout="$qout.tar"
        if ! tar cf "$qout" "$qin"; then
            debug "l3" "A problem ocurred running tar! Return code: $?"
            return 1
        else
            debug "l1" "Successfully compressed $qout!"
        fi
        ;;
        zip)
        qout="$qout.zip"
        if ! zip -9 "$qout" "$qin"; then
            debug "l3" "A problem ocurred running zip! Return code: $?"
            return 1
        else
            debug "l1" "Successfully compressed $qout!"
        fi
        ;;
        none)
        debug "l1" "No compression indicated, preparing for one-to-one copy!"
        if ! mkdir --parents "$qin"; then
            debug "l3" "Could not create directories for $qin! Does $USER have permission? Returning..."
            return 1
        fi
        if ! rsync -a "$qin"/ "$qout"; then
            debug "l3" "A problem occurred while running rsync! Return code: $?"
            return 1
        fi
        ;;
        *)
        debug "l3" "Unknown compression option $compMode set! Please fix and re-run!"
        exit 1
        ;;
    esac

    # Delete manifest if it exists
    if [[ -f "$qman" ]]; then
        rm "$qman"
    fi
    
    return 0
}

# Removes duplicates, starting from oldest file, until numBackups-1 is met
# Ignores folder if "Permanent" is detected
function dedupFolder() {
    if [[ -z $1 ]]; then
        debug "l3" "No folder given to dedupFolder()! Returning..."
        return 1
    elif [[ ! -d "$1" ]]; then
        debug "l3" "Argument $1 given to dedupFolder() is not a folder! Returning..."
        return 1
    elif [[ "$1" == *Permanent* ]]; then
        debug "l2" "Folder $1 is in a permanent location, skipping dedup processing!"
        return 0
    fi

    # Enter folder and get a list of existing files
    local tmpIndex="$(pwd)/$(date +%N).txt" # Nano seconds gives a nice, unique filename to work with 
    local OPWD="$(pwd)"
    if ! cd "$1"; then
        debug "l2" "dedupFolder() could not enter $1! Does $USER have permission? Returning..."
        return 1
    fi
    ls -1tr | tee -a "$tmpIndex" 1>/dev/null

    # Check count, and delete files from the top until quota minus one is met
    local currFile
    while [[ $(wc -l "$tmpIndex") -ge $numBackups ]]; do
        currFile="$(head -n1 "$tmpIndex")"
        debug "l2" "Removing old backup $(pwd)/$currFile..."
        if ! rm "$currFile"; then
            debug "l3" "Could not remove file $(pwd)/$currFile, does $USER have permission? Please delete manually, attempeting to continue!"
        fi
        sed -i '1d' "$tmpIndex"
    done
    
    # Done. Log, clean, and return.
    debug "l1" "Done deduping folder $1!"
    if ! cd "$OPWD"; then
        debug "l3" "Could not change back to previous directory $OPWD, does it still exist? Attempting to continue..."
    fi
    rm "$tmpIndex"
    return 0
}

function exportConfig() {
    if [[ "$doGlobal" -eq 0 ]]; then
        database="/usr/share/$(echo "$database" | rev | cut -d'/' -f 1 | rev)"
        debug "l2" "Set database to $database for global usage!"
    fi

    debug "l1" "Saving config to $database!"
    myConfig "$myConfigLocation" "prep"
    myConfig "$myConfigLocation" "add" "outputFolder"
    myConfig "$myConfigLocation" "add" "database"
    myConfig "$myConfigLocation" "add" "compMode"
    myConfig "$myConfigLocation" "add" "dateMode"
    myConfig "$myConfigLocation" "add" "numBackups"
    myConfig "$myConfigLocation" "add" "doManifest"
    myConfig "$myConfigLocation" "add" "selfBackup"
    
    if ! myConfig "$myConfigLocation" "export"; then
        debug "l3" "There was a problem exporting $myConfigLocation, check to see it was written to properly! Attempting to continue..."
        return 1
    else
        return 0
    fi
}

# Usage: priorityBackup <output_drive>
#    Script will assume external drive is already mounted here, and will copy until full
function priorityBackup() {
    # Sanity check
    if [[ -z "$1" ]]; then
        debug "l4" "No folder given to priorityBackup()! Please fix and retry!"
        return 1
    elif [[ ! -d "$1" ]]; then
        debug "l3" "$1 is not a folder! Please verify and try again!"
        return 1
    fi

    # First, make a temp copy of the database to work with
    local tmpDB="$(pwd)/tmpDB.csv"
    cat "$database" | sort -t ',' -n -k 3,3 > "$tmpDB" # Sort by priority (3rd field)

    # Now, copy over the latest of each file to this drive
    local pdir         # Directory of the key
    local pinput       # Where the name of the latest file will be held
    #local poutput      # Directory and name, based off $1
    local pkey         # The filename without the directory, used for searching
    local pspace       # Size of the input file   
    local premainder   # Remaining disk space of output directory mount
    while read -r line; do
        if [[ "$line" == \#* ]]; then
            continue
        fi
        
        pdir="$(echo "$line" | cut -d',' -f2 | rev | cut -d'/' -f1 | rev)"
        pkey="$(echo "$line" | cut -d',' -f2 | rev | cut -d'/' -f1 --complement | rev)"
        pinput="$outputFolder$pdir"
        pinput="$pinput/$(ls -1tr "$pinput" | grep "$pkey" | head -n1)" # May produce a few false positives, but I'll cross that bridge when I get there
        pspace="$(wc -c < "$pinput")"
        premainder="$(df "$1" --output=avail | grep -v Avail)"
        
        # Run checks, then copy the file
        if [[ ! -f "$pinput" ]]; then
            debug "l3" "Input $pinput is not a file! Skipping..."
        elif [[ $pspace -ge $premainder ]]; then
            debug "l3" "Not enough space on folder mounted at $1 ($pspace needed, $premainder remaining)! Skipping item..."
        elif ! cp "$pinput" "$1"; then
            debug "l3" "Could not copy $pinput to $1, does $USER have permission? Please fix and re-run!"
        else
            debug "l1" "Successfully copied $pinput to $1!"
        fi
    done < "$tmpDB"

    if [[ -f "$tmpDB" ]]; then
        if ! rm "$tmpDB"; then
            debug "l2" "Could not delete temp database $tmpDB, please do so manually!"
        fi
    fi

    debug "l1" "Done backing up database to $1!"
    return 0    
}

### Main

if ! myConfig "import"; then
    debug "l2" "Could not import config, starting setup!"
fi

if ! processArgs "$@"; then
    debug "l4" "A problem occurred while processing arguments, exiting!"
    displayHelp
    exit 1
fi

# Let the record show I worked on this on the 21st day of the 21st year of the 21st century through the 21st minute of the 21st hour. #NotABoomer

# Make sure output directory exists, then run and report!
if ! mkdir --parents "$outputFolder"; then
    debug "l4" "Unable to make directories for $outputFolder, does $USER have permission? Please fix and re-run!"
    exit 1
elif ! processDatabase; then
    debug "l3" "An error occurred while processing the database at $database! Please check log and try again!"
    exit 1
else
    debug "l1" "Done with script!"
fi

#EOF