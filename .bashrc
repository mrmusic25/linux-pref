# Things that I would like added to my .bashrc list
# NOTE: This is NOT a replacement for the actual .bashrc, only do so if you know what you are doing!

# Import commonVars.sh if it can be located
if [[ -f "commonVars.sh" ]]; then
	source "commonVars.sh"
elif [[ -f "/usr/share/commonVars.sh" ]]; then
	source "/usr/share/commonVars.sh"
else
	echo "WARN: Could not find commonVars.sh, functions may not work as intended!" 1>&2
fi

# A 'very popular' function. Looks helpful!
function extract()
{
     if [[ -f "$1" ]] ; then
         case "$1" in
            *.tar.bz2)   
                tar xvjf "$1"
				return $?     
                ;;
            *.tar.gz)    
                tar xvzf "$1"
				return $?      
                ;;
            *.bz2)       
                bunzip2 "$1"
				return $?       
                ;;
            *.rar)
                unrar x "$1"
				return $?       
                ;;
            *.gz)
                gunzip "$1" 
				return $?       
                ;;
            *.tar)
                tar xvf "$1"  
				return $?     
                ;;
            *.tbz2)
                tar xvjf "$1" 
				return $?     
                ;;
            *.tgz)
                tar xvzf "$1"   
				return $?   
                ;;
            *.zip)
                unzip "$1"  
				return $?       
                ;;
            *.Z)
                uncompress "$1"  
				return $?  
                ;;
            *.7z)
                7z x "$1"      
				return $?    
                ;;
            *)  
                echo "$1 cannot be extracted via extract" >&2
				return 1
                ;;
        esac
    else
        echo "$1 is not a valid file" >%2
		return 1
    fi
}

# Uncomment the line below if aliases do not work. Change to '/root/.bash_aliases' for root user
if [[ -f "$HOME/.bash_aliases" ]]; then
	source "$HOME/.bash_aliases"
fi

# The following two lines make nano the default editor. Comments if you use vim or others, or change it
export VISUAL="nano"
export EDITOR="nano"

# A fun little program I found on Reddit one day, change city to official list at wttr.in!
wttrCity="los_angeles"
alias weather='curl wttr.in/$wttrCity'

function push() {
	# If arg provided, use that as directory. Otherwise, assume pwd
	if [[ -z "$1" ]]; then
		if [[ ! -d .git ]]; then
			echo "$(pwd) is not a valid git directory!" >&2
			return 1
		else
			git push
			local rval=$?
		fi
	else
		if [[ ! -d "$1"/.git ]]; then
			echo "$1 is not a valid git directory!" >&2
			return 1
		else
			local OPWD="$(pwd)"
			cd "$1"
			git push
			local rval=$?
			cd "$OPWD"
		fi
	fi
	return $rval
}

function pull() {
	# If arg provided, use that as directory. Otherwise, assume pwd
	if [[ -z "$1" ]]; then
		if [[ ! -d .git ]]; then
			echo "$(pwd) is not a valid git directory!" >&2
			return 1
		else
			git pull
			local rval=$?
		fi
	else
		if [[ ! -d "$1"/.git ]]; then
			echo "$1 is not a valid git directory!" >&2
			return 1
		else
			local OPWD="$(pwd)"
			cd "$1"
			git pull
			local rval=$?
			cd "$OPWD"
		fi
	fi
	return $rval
}

function commit() {
	# If arg provided, use that as directory. Otherwise, assume pwd
	if [[ -z "$1" ]]; then
		if [[ ! -d .git ]]; then
			echo "$(pwd) is not a valid git directory!" >&2
			return 1
		else
			git commit -a
			local rval=$?
		fi
	else
		if [[ ! -d "$1"/.git ]]; then
			echo "$1 is not a valid git directory!" >&2
			return 1
		else
			local OPWD="$(pwd)"
			cd "$1"
			git commit -a
			local rval=$?
			cd "$OPWD"
		fi
	fi
	return $rval
}

# Got the idea for this one day
if [[ -z $debugPrefix ]]; then
	if [[ -z $lpDir ]]; then
		echo "WARN: lpDir not set! Guessing $HOME/.config/linux-pref/logs! Legacy setups may not display properly!"
		export logDir="$HOME/.config/logs"
	else
		export logDir="$lpDir/logs"
	fi
else
	export logDir="$debugPrefix"
fi

function log() {

read -d '' logUsage << endHelp
Usage: log <logFile> [numLines] OR log <command>
Type 'log command' to see supported commands.
numLines is the number of lines you would like to be read (via tail).
endHelp

	if [[ -z $1 ]]; then
		printf "ERROR: No arguments given with log()!\n\n%s\n" "$logUsage" >&2
		return 1
	fi

	case $1 in
		-h|--help|help)
		printf "%s\n" "$logUsage"
		return 0
		;;
		ls|list)
		printf "Displaying files in %s...\n" "$logDir"
		ls -l "$logDir"
		return 0
		;;
		dir|display|show)
		printf "Log directory is located at: %s\nType in \'log cd\' to switch to the directory!\n" "$logDir"
		return 0
		;;
		cd|switch)
		printf "Changing current directory to %s!\n" "$logDir"
		cd "$logDir"
		return 0
		;;
		command*)
		printf "Commands:\n  ls|list          : List the files in the set log directory\n  cd|switch        : Switch to the set log directory\n dir|display|show  : Tells you the name of the set log directory\n"
		return 0
		;;
		*)
		true # Used to be an error here, but now with file guessing it is irrelevent
		;;
	esac

	if [[ ! -z $2 && "$2" -eq "$2" ]]; then
		nlines="$2"
	else
		nlines="20"
	fi
	
	OPWD="$(pwd)"
	cd "$logDir" # Makes searching easier
	
	if [[ -e "$1" ]]; then
		logRead="$1"
	else # Partial name, guess the rest
		printf "WARN: Partial name given for log file, guessing the rest!\n" >&2
		logRead="$(ls *.log | grep "$1")"
	fi
	
	printf "INFO: Showing the last %s lines of log file %s:\n\n" "$nlines" "$logRead"
	tail -n "$nlines" "$logRead"
	cd "$OPWD"
	return 0
}

# Tired of starting a graphical program from the terminal but hate the stdout? Daemonize it!
# Runs 'eval' on all output, and runs it as a daemon
# Fun fact: I came up with this fun name while watching Supernatural, lol
function daemonize() {

read -d '' daemonizeUsage << endHelp
Usage: daemonize <program> [arguments]
All arguments will be run as given.
Use this function to run any function as a daemon.
endHelp

	if [[ -z $1 ]]; then
		printf "ERROR: No arguments given with daemonize()!\n\n%s\n" "$daemonizeUsage"
		return 1
	fi
	
	# Only real requirement. Run everything else in an eval statement
	eval "$@" &>/dev/null 2>&1 &disown;
}

# Whenever I reinstall Linux, I end up setting lots of shorhand aliases to my Git directories
# Now, with this function, I can save myself time setting these up!
# alink == alias-link; all aliases stored in $HOME/.alink and imported to .bashrc
function alink() {

read -d '' alinkUsage << endHelp
Usage: alink <shortcut>
Running this command will create an alias to $1 in .bashrc
Useful for frequently worked on folder. You can use LP to stand for /linux-pref, for example
This makes it so later all you have to do is run 'cd $LP' instead of the full directory name
endHelp

	if [[ -z $1 ]]; then
		printf "ERROR: No argument given to alink()!\n\n%s\n" "$alinkUsage" >&2
		return 1
	elif [[ "$1" == "-h" || "$1" == "--help" ]]; then
		printf "%s\n\n" "$alinkUsage" >&2
		return 0
	elif [[ ! -f "$HOME"/.alink ]]; then
		printf "WARN: \$HOME/.alink does not exist! Creating and linking to .bashrc!\n" >&2
		touch "$HOME"/.alink
		
		printf "\n# Sources .alink from linux-pref, if it exists\nif [[ -f \"\$HOME/.alink\" ]]; then\n   source \"\$HOME/.alink\"\nfi\n" | tee -a "$HOME/.bashrc" 1>/dev/null
	fi
	
	printf "%s=%s\n" "$1" "$(pwd)" | tee -a "$HOME/.alink" 1>/dev/null
	source "$HOME/.alink" # That way new link is immediately added to env
}

# Copies 'scriptTemplate.sh' to current folder, if it can be found in $LPLOC
# If name of new script is given as $1, use it
function newScript() {
	# Make sure function has info to run
	if [[ -n $LPLOC || ! -d "$LPLOC" || ! -f "$LPLOC/scriptTemplate.sh" ]]; then
		echo "ERROR: Could not find scriptTemplate.sh in linux-pref (=$LPLOC) directory!" 1>&2
		return 1
	fi

	# Determine output name
	local oname="$(pwd)"
	if [[ -n $1 ]]; then
		echo "INFO: No name given to newScript(), copying template as-is to current directory!"
	elif [[ "$1" != *.sh ]]; then
		oname="$oname/$1.sh"
	else
		oname="$oname/$1"
	fi

	# Copy and report
	echo "INFO: Copying $LPLOC/scriptTemplate.sh to $oname!"
	if ! cp "$LPLOC/scriptTemplate.sh" "$(pwd)/$oname"; then
		echo "ERROR: Couldn't copy $LPLOC/scriptTemplate.sh to $(pwd)/$oname, does $USER have permission? Giving up..." 1>&2
		return 1
	fi

	return 0
}

# Quick function to add status to dd
function dd() {
	if [[ -z $1 ]]; then
		echo "ERROR: No arguments given to dd!"
		eval "$(which dd)" --help # Otherwise it will just call function again
	else
		eval "$(which dd)" "$@" status=progress
		return $?
	fi
}