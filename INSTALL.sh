#!/usr/bin/env bash
#
# INSTALL.sh - A script meant to automatically install and setup my favorite options
# Usage: ./INSTALL.sh [options] <install_option>
# Run with the -h|--help option to see full list of install options (or look at displayHelp())
#
# Changes:
# v3.0.1
# - Script now outputs location of install folder to cfVars
# - Switched to using 'eval' to call packageManager, since it hasn't worked lately
#
# v3.0.0
# - Yet another re-work of this script, first revision in two years apparently...
# - Went back to old style of using variables to install parts of program
# - Updated all debug() statements to new API
# - Removed grive refrences as it is now archived
# - Also removed gitManager refrences, but script now sets up git config in its place
# - Added more checks and balances, much like the 2020 election did
# - Fixed root password set not working properly
# - Added support for new $lpDir and commonVars.sh
# - Everything linux-pref related in it's own public directory now for easy cleanup
# - Removed /etc/skel code since I never use it anyways
#
# v2.1.2
# - Added some code to remind the user to set the root password in case of emergency
# - Made shortName and longName use export, testing a theory mostly
#
# v2.1.1
# - Don't really have a way to test the uninstall() feature, but it is now implemented
#
# v2.1.0
# - Fixed a problem keeping skel installation from working in installBash()
# - Added backup function to installBash() for user, root, and skel
# - Made rmLink() to make removing links at the end of uninstall() easier
# - Finished writing uninstall()
# - Fixed issue with cut for -o|--only and -e|--except
#
# v2.0.3
# - More work on uninstall()
#
# v2.0.2
# - Got rid of adding cronjob in this script for gm; already has that implemented
# - Started work on uninstall()
#
# v2.0.1
# - Added all the installation functions, wrote them all (read: wrote one, then copy+pasted the rest)
# - Added folder check
# - All functions except grive ready for testing
# - Backup for crontab enabled
# - No need to backup .bashrc now because of new sourcing method
#
# v2.0.0
# - Started work on re-written, more efficient install script
# - Use 0b664c6 to get original script, most of it was erased after that commit
# - Wrote displayHelp and supporting variables/processArgs() fills
# - Added a monstorosity of a switch to decide which options to run
#
# TODO:
#
# v3.0.1, 22 Jan. 2021, 00:38 PST

### Variables

export longName="INSTALL" #shortName and longName used for logging
export shortName="iS" # installScript
export sudoCheck=0 # Tells script whether to ignore sudo warning
export run=0 # Indicates whether or not scripts will be run by default
export ask=0 # Whether or not user will be asked before running each script

# Vars for whether or not functions should be installed
export doinstallBash=1
export doinstallPM=1
export doinstallPrograms=1
export doinstallGit=1

### Functions

# Used to link the various common files to /usr/share for everyone to use
function linkCF() {
	declare -a files=("commonVars.sh" "commonFunctions.sh" "packageManagerCF.sh")

	for file in "${files[@]}"; do
		if [[ ! -f "$file" ]]; then
			echo "ERROR: $file not found in current directory, please correct and re-run!"
			echo "Please cd into the directoy containing $0 and $file!"
			exit 1
		fi
		
		# Give sudo warning if not root
		if [[ "$EUID" -ne 0 ]]; then
			echo "WARN: Linking $file to /usr/share requires root permissions, please login" >&2
		else
			echo "INFO: Linking $file to /usr/share!" >&2
		fi

		# Make sure file actually links
		if ! sudo ln -sf "$(pwd)/$file" "/usr/share/$file"; then
			echo "FATAL: Could not link $file to /usr/share! Please diagnose and retry!" >&2
			exit 2
		fi
		
		source "$file"
	done

	if [[ "$(pwd)" == *linux-pref ]]; then
		echo "export LPLOC=$(pwd)" >> "$HOME/.bashrc"
	else
		echo "WARN: Script is being run outside normal linux-pref directory! Not linking to .bashrc!"
	fi
}

if [[ -f /usr/share/commonFunctions.sh ]]; then
	source /usr/share/commonFunctions.sh
elif [[ -f commonFunctions.sh ]]; then
	linkCF
else
	echo "commonFunctions.sh could not be located!"
	echo "Script will now exit, please put file in same directory as script, or link to /usr/share!"
	exit 1
fi

function displayHelp() {
# The following will read all text between the words 'helpVar' into the variable $helpVar
# The echo at the end will output it, exactly as shown, to the user
read -d '' helpVar <<"endHelp"

INSTALL.sh - A script that sets up a new computer with other scripts and settings from mrmusic25/linux-pref

Usage: ./INSTALL.sh [options] <install_option>

Options:
  -h | --help                         : Display this help message and exit
  -v | --verbose                      : Prints verbose debug information. MUST be the first argument!
  -s | --sudo                         : Ignores sudo-check (use this option if root is only user being used on system)
  -n | --no-run                       : Only setup links, don't run any of the scripts
  -a | --ask                          : Ask before running each script

Install Options (iOptions):
  all                                 : Installs all the below options/scripts
  pm | packageManager                 : Universal package manager script
  programs                            : Installs programs from the local programLists folder
  git                                 : Updates git settings
  bash                                : Installs .bashrc and .bash_aliases for selected users (will be asked)
  uninstall                           : Uninstalls all scripts, links, and files

endHelp
echo "$helpVar"
}

function processArgs() {
	# displayHelp and exit if there is less than the required number of arguments
	# Remember to change this as your requirements change!
	if [[ $# -lt 1 ]]; then
		debug "l3" "No arguments given! Please fix and re-run"
		displayHelp
		exit 1
	fi
	
	# This is an example of how most of my argument processors look
	# Psuedo-code: until condition is met, change values based on input; shift variable, then repeat
	while [[ $loopFlag -eq 0 ]]; do
		key="$1"
			
		case "$key" in
			-h|--help)
			displayHelp
			exit 0
			;;
			-v|--verbose)
			debugFlag=1
			debugLevel=2
			debug "l2" "Vebose mode enabled! In the future, however, please make -v|--verbose the first argument!"
			;;
			-s|--sudo)
			sudoCheck=1
			;;
			-n|--no-run)
			run=1
			debug "l1" "User has indicated not to run scripts"
			;;
			-a|--ask)
			ask=1
			debug "l1" "User will be asked before each script is run"
			;;
			-e|--except)
			if [[ -z $2 ]]; then
				debug "l4" "No arguments given with $key, please fix and re-run!"
				exit 1
			fi
			specific="except"
			loopFlag=1 # Assume the rest is install options and continue
			;;
			-o|--only)
			if [[ -z $2 ]]; then
				debug "l4" "No arguments given with $key, please fix and re-run!"
				exit 1
			fi
			specific="only"
			loopFlag=1
			;;
			#--dry-run)
			#dryRun="set" # Oh, a secret argument! When run, script will only output anticipated changes/debug messages without making changes
			#debug "l1" "WARN: Doing a dry-run, nothing will be changed!"
			#;;
			*)
			# If it is not an option, assume it is the run mode and continue
			loopFlag=1
			;;
		esac
		shift
	done
}

function installPM() {
	debug "l1" "Installing packageManager.sh and related functions!"
	
	# Make sure script is here
	if [[ ! -f packageManager.sh || ! -f packageManagerCF.sh ]]; then
		debug "l4" "packageManager.sh or packageManagerCF.sh could not be found! Could not be successfully installed!"
		return 1
	fi
	
	dynamicLinker "$(pwd)/packageManager.sh" /usr/bin
	#dynamicLinker "$(pwd)/packageManagerCF.sh" /usr/share
	
	if [[ $run -ne 0 ]]; then
		debug "l1" "User indicated not to run scripts! Moving on..."
	elif [[ $ask -ne 0 ]]; then
		debug "l1" "User has chosen to be asked about running scripts, asking for confirmation..."
		getUserAnswer "pm.sh has been installed, would you like to update your computer now?"
		case $? in
			0) #true
			debug "l1" "User indicated to run pm.sh!"
			# continue with function
			;;
			1)
			debug "l1" "User chose not to run script, returning!"
			return 0 # technically a success
			;;
		esac
	fi
	
	# If you made it this far, script is meant to be run
	debug "l2" "Now running pm.sh and upgrading current system!"
	if [[ -z $(which pm) ]]; then
		debug "l3" "Link to packageManager.sh was not found! Please fix manually and re-run!"
		return 1
	elif ! eval pm fuc; then
		debug "l3" "A problem occured while running packageManager.sh! Check logs for more info. Attempting to continue... Code: $?"
		return 1
	else
		debug "l1" "Done installing and running pm.sh!"
	fi
	return 0
}

function installPrograms() {
	# Check to see if function should even be run
	if [[ $run -ne 0 || $ask -ne 0 ]]; then # Two birds, one stone
		debug "l2" "User asked not to run scripts, but indicated to install programs!"
		getUserAnswer "n" "Would you like to install programs anyways?"
		case $? in
			0)
			true # debug message below, no need to duplicate it
			;;
			*)
			debug "l1" "User indicated not to install programs, returning..."
			return 0
			;;
		esac
	fi
	
	# Making it this far means user wants programs installed
	debug "l1" "Installing programs in programLists/ folder!"
	if [[ -z $(which pm) ]]; then
		debug "l3" "Link to packageManager.sh was not found! Please fix manually and re-run!"
		return 1
	elif ! eval pm i programLists/; then
		debug "l3" "An error ocurred while installing files in programLists/! Code: $?, attempting to continue..."
		return 1
	else
		debug "l1" "Done installing programs!"
	fi
	return 0
}

function installGit() {
	announce "Setting up Git for this system!" "Please follow the interactive prompts."
	read -p "Please enter the email you would like to use: " varEmail
	git config --global user.email "$varEmail"
	read -p "Please enter the display name you would like to use: " varUser
	git config --global user.name "$varUser"
	read -p "Please enter the default editor you wish to use (nano, vim, emacs, etc.): " varEditor
	git config --global core.editor "$varEditor"
	debug "INFO: Git set up as follows - Name: $varUser, Email: $varEmail, Editor: $varEditor"
	
	# Static config now
	git config --global credential.helper store
	debug "INFO: Git credential storage enabled"
	announce "Git credential storage has been enabled." "This means you will only have to login once to upload merges"
	git config --global push.default simple
	debug "INFO: Simple push behavior has been enabled"
	announce "Git has been configured to use simple push behavior (default since git v2.0+)" "This means that only the current working branch will be pushed, not all!"

	git config --global push.followTags true
	git config --global color.ui auto

	debug "l1" "Done setting up git options!"
}

function installBash() {
	# No run not affected here, only check if ask is set
	if [[ $ask -ne 0 ]]; then
		debug "l1" "User indicated to be asked before running scripts, asking before installing bash"
		getUserAnswer "Would you like to install .bashrc and .bash_aliases?"
		case $? in
			0)
			debug "l1" "Installing .bashrc and .bash_aliases per user's choice (will confirm for root)"
			;;
			*)
			debug "l1" "User chose not to install bash"
			return 0
			;;
		esac
	fi
	
	local dir
	if [[ ! -z $lpDir && -f "$lpDir" ]]; then
		dir="$lpDir"
	else
		dir="$HOME/.config/linux-pref"
	fi
	mkdir -p "$dir"
	chmod -R 0755 "$dir"

	# Proceed with installation
	# After making sure some stuff exists first
	if [[ ! -e "$HOME/.bashrc" ]]; then
		touch "$HOME/.bashrc"
	fi
	
	touch "$dir/.lp" # Got smart, this make it so link sent to .bashrc are no longer deleted upon uninstallation
	chmod 755 "$dir/.lp" # Since others may be using it

	cp "$HOME"/.bashrc "$HOME"/.bashrc.bak # Gotta do those backups
	printf "# Locations of .bashrc and .bash_aliases as added by linux-pref on %s\nsource %s\nsource %s\n" "$(date)" "$(pwd)/.bashrc" "$(pwd)/.bash_aliases" >> "$dir"/.lp
	
	printf "# Added by linux-pref to import .bashrc and .bash_aliases from git repo\nexport lpDir=%s\nif [[ -f \"\$lpDir/.lp\" ]]; then\n   source \"\$lpDir/.lp\"\nfi\n" "$dir" >> "$HOME"/.bashrc
	
	# Never had a need to the /etc/skel function, but don't want to delete. Needs updating if/when it gets uncommented
	#if [[ ! -d /etc/skel ]]; then
	#	debug "l2" "skel directory not found! Unable to install bash for future users! Continuing..."
	#else
	#	getUserAnswer "n" "Would you like to install bash for future users through the skel directory?"
	#	case $? in
	#		0)
	#		debug "l1" "INFO: Installing bash to skel directory as user indicated"
	#		debug "l2" "WARN: Installing bash to skel directory will require sudo premission!"
	#		sudo touch /etc/skel/.lp
	#		sudo cp /etc/skel/.bashrc /etc/skel/.bashrc.bak 2>/dev/null # Suppress warning if .bashrc doesn't exist, like on some systems
	#		printf "# Locations of .bashrc and .bash_aliases as added by linux-pref on %s\nsource %s\nsource %s\n" "$(date)" "$(pwd)/.bashrc" "$(pwd)/.bash_aliases" | sudo tee -a /etc/skel/.lp > /dev/null
	#		printf "# Added by linux-pref to import .bashrc and .bash_aliases from git repo\nif [[ -f .lp ]]; then\n   source .lp\nfi\n" | sudo tee -a /etc/skel/.bashrc > /dev/null
	#		;;
	#		*)
	#		debug "l1" "INFO: Skipping installation to skel directory..."
	#		;;
	#	esac
	#fi
	
	if [[ $sudoCheck -eq 0 ]]; then
		debug "l1" "Asking to install bash for root user"
		getUserAnswer "Would you like to install .bashrc and aliases for root user?"
		case $? in
			0)
			debug "l1" "Installing .bashrc and .bash_aliases for root user"
			debug "l2" "Installing bash for root will require sudo premission!"
			sudo touch /root/.lp
			sudo cp /root/.bashrc.bak 2>/dev/null # Suppressed for same reason as skel
			
			#printf "# Locations of .bashrc and .bash_aliases as added by linux-pref on %s\nsource %s\nsource %s\n" "$(date)" "$(pwd)/.bashrc" "$(pwd)/.bash_aliases" | sudo tee -a /root/.lp > /dev/null
			printf "# Added by linux-pref to import .bashrc and .bash_aliases from git repo\nexport lpDir=%s\nif [[ -f \$\"lpDir/.lp\" ]]; then\n   source \"\$lpDir/.lp\"\nfi\n" "$dir" | sudo tee -a /root/.bashrc 1> /dev/null
			;;
			*)
			debug "l1" "User indicated not to install bash for root user!"
			;;
		esac
	else
		debug "l1" "sudoCheck disabled, assuming root user has already been installed and continuing!"
	fi
	return 0
}

function uninstall() {
	debug "l2" "Uninstalling linux-pref from the system!"
	announce "NOTE: EVERYTHING will be uninstalled after running this!" "If you only want a couple things installed, you will have to reinstall them manually" "Press CTRL+C now to stop uninstallation"
	
	local dir
	if [[ ! -z $lpDir && -f "$lpDir" ]]; then
		dir="$lpDir"
	else
		dir="$HOME/.config/linux-pref"
	fi

	# Restore crontab (and grive)
	if [[ ! -e "$HOME"/.crontab.bak ]]; then
		debug "l2" "No crontab backup found! Please fix cron manually using \"crontab -e\"!"
	else
		debug "Attempting to restore crontab from backup..."
		announce "Be careful with this step! Cron jobs could be messed up!" "Script will attempt to restore original crontab" "If you have custom jobs, please fix manually"
		getUserAnswer "Would you like to edit cron manually? (Hint: 'No' will restore from backup)"
		if [[ $? -eq 0 ]]; then
			debug "User has decided to manually edit crontab"
			announce "As you wish, manually editing crontab!" "Remove all linux-pref scripts from the list, or comment them out!"
			crontab -e
		else
			debug "User has been warned, now attempting to restore original crontab"
			crontab "$HOME"/.crontab.bak
			val="$?"
			if [[ $val -ne 0 ]]; then
				debug "l2" "Crontab restoration was not successful! Error code: $val"
			fi
		fi
	fi
	
	# Can't think of a good way to uninstall programs, so just warn the user and move on
	debug "l2" "Script will not uninstall programs from package manager, please do so manually"
	
	# Ask to restore .bashrc.bak, if it exists
	if [[ -e "$HOME"/.bashrc.bak ]]; then
		debug ".bashrc.bak found, asking user to restore"
		announce ".bashrc.bak was found! Script will try to restore it!" "WARN: This could get rid of other aliases and custom functions!" "You can choose not to restore with no consequences or errors"
		getUserAnswer "Would you like script to restore .bashrc.bak?"
		if [[ $? -eq 0 ]]; then
			debug "l1" "User chose to restore .bashrc, attempting to do so!"
			rm -v "$HOME"/.bashrc
			mv -v "$HOME"/.bashrc.bak "$HOME"/.bashrc
		else
			debug "l2" "User chose not to restore .bashrc, deleting .bashrc.bak"
			rm -v "$HOME"/.bashrc.bak
		fi
	else
		debug "l3" ".bashrc backup not found! Cannot restore!"
	fi

	# Remove .lp ifpresent. no need to confirm with user
	debug "INFO: Removing .lp if it exists"
	rm "$dir"/.lp 2>/dev/null # Didn't feel like writing an if statement for this
	
	# Restore /etc/skel
	debug "l2" "INFO: Attempting to restore bash for root directory, requiring sudo permissions!"
	#if [[ -e /etc/skel/.bashrc.bak ]]; then
	#	debug "l2" "WARN: .bashrc backup found in /etc/skel, restoring..."
	#	sudo rm -v /etc/skel/.bashrc
	#	sudo mv -v /etc/skel/.bashrc.bak /etc/skel/.bashrc
	#fi
	
	# Do the same for root. Little more complicated as it requires sudo
	if [[ ! -z $(sudo cat /root/.bashrc.bak) ]]; then
		debug "l2" ".bashrc backup found in /root, restoring..."
		sudo rm -v /root/.bashrc 2>/dev/null # No reason it shouldn't be there, but suppress error just in case
		sudo mv -v /root/.bashrc.bak /root/.bashrc
	else
		debug "l2" "No backup found for root! Shouldn't cause any issues, but beware!"
	fi
	
	# Remove the linux-pref dir, if it exists
	if [[ -f "$dir" ]]; then
		debug "l2" "Now removing the linux-pref directory from $HOME/.config and all subdirectories! sudo required for sanity."
		if ! rm -rf "$dir"; then
			printf "FATAL: Something went wrong setting permissions! Please delete %s manually!\n" "$dir" >&2
		fi
	fi
			
	# Finally, remove all symlinks
	declare -a files=("commonVars.sh" "commonFunctions.sh" "packageManagerCF.sh")
	for file in "${files[@]}"; do
		if ! sudo rm "$file"; then
			printf "ERROR: Unable to delete link %s! Please fix manually!\n" "$file" >&2
		fi
	done

	echo "Done uninstalling everything, directory can safely be deleted. Thanks for using my scripts! Hope to see you again soon!"
	exit 0
}

# Accepts input of a link. Returns 0 for success, 1 for error. No debug, since it runs at the end of uninstall()
function rmLink() {
	if [[ -z $1 ]]; then
		echo "ERROR: Incorrect call for rmLink()!"
		return 1
	fi
	
	if [[ ! -L "$1" ]]; then
		echo "ERROR: $1 is not a symbolic link! Not removing"
		return 1
	else
		echo "WARN: Removing symbolic link at $1"
		sudo rm -v "$1"
		return $?
	fi
	return 0 # Not necessary, but I like to be consistent
}

### Main Script

processArgs "$@"

# First, make sure user isn't sudo
if [[ $EUID -eq 0 ]]; then
	if [[ $sudoCheck -ne 0 ]]; then
		debug "l2" "Ignoring sudo check, continuing with script as root!"
	else
		announce "This script is meant to be run as a normal user, not root!" "Please run the script without sudo." "Or, if root is the only user, run with the -s|--sudo option!"
		debug "l1" "Warned user about using script as sudo, exiting..."
		exit 1
	fi
fi

# Check to make sure pwd is git directory
if [[ "$(pwd)" != *linux-pref ]]; then
	debug "l4" "Current directory is incorrect! Please re-run script inside linux-pref folder! Exiting..."
	exit 1
fi

# Verify home dir
if [[ -z $HOME ]]; then
		HOME="$(echo ~)"
fi

# Check if computer is Raspberry Pi
if [[ -e "/usr/bin/raspi-config" ]]; then
		announce "Raspberry Pi detected!" "If this is the first time being run, please make sure locale is correct!" "Also make sure SSH is enabled in advanced options!"
		getUserAnswer "Would you like to run raspi-config before running the rest of the script?"
		case $? in
			0)
			debug "l1" "User chose to run raspi-config"
			sudo raspi-config
			;;
			*)
			debug "l1" "User chose not to run raspi-config!"
			;;
		esac
fi

# If uninstall is the argument, run it! Always confirm with user first, though!
if [[ "$1" == un* ]]; then
	debug "User has indicated to uninstall everything! Confirming..."
	getUserAnswer "WARNING: You have indicated to uninstall linux-pref! Would you like to continue?"
	case $? in
		0)
		debug "l1" "User has been warned, continuing with uninstallation"
		uninstall
		exit 0
		;;
		*)
		debug "l2" "User chose not to uninstall, exiting!"
		displayHelp
		exit 0
		;;
	esac
fi

# Now, for actual installation
# Backup crontab, just in case it is used
if [[ ! -e "$HOME"/.crontab.bak ]]; then
	debug "l2" "Creating backup of current user's crontab at $HOME/crontab.bak"
	crontab -l > "$HOME"/.crontab.bak
else
	debug "l2" "Backup of crontab already exists at $HOME/crontab.bak"
	getUserAnswer "n" "Would you like to overwrite the backup?"
	case $? in
		0)
		debug "l1" "Overwriting crontab backup at user's request"
		crontab -l > "$HOME"/.crontab.bak
		;;
		*)
		debug "l1" "Skipping crontab backup, as one already exists"
		;;
	esac
fi

# Now, install everything
# Remaining arguments should be the things to install. Just make sure to do them in the right order
while [[ ! -z $1 ]]; do
	case $1 in
		all)
			doinstallPM=0
			doinstallPrograms=0
			doinstallGit=0
			doinstallBash=0
			;;
		pm|pac*)
			doinstallPM=0
			;;
		pr*)
			doinstallPrograms=0
			;;
		g*)
			doinstallGit=0
			;;
		b*)
			doinstallBash=0
			;;
		-*)
			debug "l1" "$1 is an option, ignoring..."
			;;
		*)
			debug "l3" "Given option $1 not valid! Please fix and re-run!"
			displayHelp
			exit 1
			;;
	esac
	shift
done

# S P A G H E T T I
if [[ $doinstallBash -eq 0 ]]; then
	installBash
fi

if [[ $doinstallGit -eq 0 ]]; then
	installGit
fi

if [[ $doinstallPM -eq 0 ]]; then
	installPM
fi

if [[ $doinstallPrograms -eq 0 ]]; then
	installPrograms
fi

# To avoid complications, ask user to set root password
if getUserAnswer "Would you like to set the root password?"; then
	debug "l1" "Setting root password per user request!"
	announce "Please set the root password at the following prompt"
	sudo passwd root
fi

debug "l1" "Done swtting everything up!"
announce "Done setting up linux-pref!" "It is recommended to close and re-open any terminals for it to take effect."

exit 0

#EOF
