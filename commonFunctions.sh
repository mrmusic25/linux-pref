#!/usr/bin/env bash
#
# commonFunctions.sh - A collection of functions to be used in this project
# Note: this script whould not be run by itself, as it only contains functions and variables
#
# Changes:
# v2.0.7
# - Fixed boolean error with yesorno() and isBoolean(), not liking the switch from ! -z to -n...
#
# v2.0.6
# - Fixed yesorno() to correctly handle given string
# - Added quick isBoolean() function using same names as yesorno() to identify correct user input
# - Changed getUserAnswer() to use new functions
#
# v2.0.5
# - announce() now waits in verbose mode
# - Also added option to turn off waiting in announce() for both forms
#
# v2.0.4
# - Undid the noSha changes from v2.0.1 becuase I'm dumb and don't read my own documentation
# - Changed indexFolder() to use ls -a so dotfiles are supported now
#
# v2.0.3
# - Added yesorno() to help determine value of strings
#
# v2.0.2
# - Improved 'global' usage of myConfig()
# - Permissions of file will now change for myConfig() for ease of use
#
# v2.0.1
# - Added 'secure' options to myConfig() to change permissions to user-only
# - Added option to disable SHA-256 hashes for indexFolder()
#
# v2.0.0
# - Overhaul of basically every function, mostly QoL improvements
# - Variables now stored in new file commonVars.sh (that way personal config can be implemented later)
# - debug() now function more like my C++ implementation, calls will have to be changed to match it now
# - Removed ctrl_c() because it implementation never worked
# - Also removed checkPrivilege() because of lack of usage. Will replace.
# - Gave announce() some QoL improvements, as well as colors
# - Made getUserAnswer() more efficient, and added color as well
# - Changed checkout() to use $lpDir, and added a check for mkdir
# - dynamicLinker() and myConfig() also changed to use $lpDir
# - Had to change myConfig() to use $shortName, might mess up some stuff...
# - All internal debug() messages updated to new format, other scripts must now be changed
#
# v1.14.3
# - Added time output to debug() 
# - Output from debug() now colored for readability!
#
# v1.14.2
# - newFileScan() is done and ready to be tested!
#
# v1.14.1
# - Removed recursive function of newFileScan(), would have been to hard to implement and couldn't find good use case
# - More work on newFileScan()
#
# v1.14.0
# - Started work on newFileScan()
#
# v1.13.5
# - Slightly changed implementation of $aChar so it doesn't get easily overwritten
# 
# v1.13.4
# - Added ability to use a global config file with myConfig()
# - All you have to do is add a filename to the beginning of normal myConfig() commands
# - Changed checkout() to use a local var, making life easier
# - checkout() also uses $HOME/.lpconf now, to make things even more simple
# - Removed some hilariously old TODO stuff
# - NOTE: Paying attention to the commit/revision times tells you a lot about a programmer
# - Minor text fixes
#
# v1.13.3
# - Added a comment function to myConfig()
# - Added a remove function to MyConfig() to 'delete' old config in prep for new one
# - Rookie mistake, forgot to add quotations in case of spaces
#
# v1.13.2
# - More functional changes, should be working now
# - Added a "first run" to myConfig() to delete old temp config, if it exists
# - I was already practicing variable expansion, so made a simple yet effective change to myConfig()
# - Implemented a changed I had been wanting for wuite a while - now announce() can have it's border char configured!
# - To use, simply add the line 'export aChar="<char>"' to your script after sourcing commonFunctions!
# - Only the first char of the $aChar will be used, however
#
# v1.13.1
# - As per usual, forgot to run Shellcheck. Fixed functional problems
# - Added a 'prep' option to myConfig() to simply ready the files for use later
# - Minor text fixes
#
# v1.13.0
# - Added the function myConfig()
# - Allows user to store a config file for each specific script for commonly used settings
# - See documentation for more info!
#
# TODO:
# - Add hello()/bye() OR script(start/stop) function to initialize scripts
#   ~ Start debugger, log the start time, source files that are needed according to script
#     ~ Each script has "sourceVar" which is an array of required scripts to source before running. Quit if can't be found.
#   ~ Put exit debug message, "Script completed in x.yz seconds", announce "Done with script!" or "Script exited successfully!"
#     ~ Exit with included code, don't print success message if code > 0
# - announce()
#   ~ If announce() reaches $MAX_CHAR_LIMIT, output extra data to second line
#   ~ Disable stars and printf statements when -v|--verbose is on, makes debugging cleaner
# - timeDifference()
#   ~ Display (and possibly log) the difference between two times. Thought of for m2u
#     ~ Possibly just add to debug()? Or finally in the implementation of 'script start' or 'script end <exit_code>'
#   ~ http://stackoverflow.com/questions/8903239/how-to-calculate-time-difference-in-bash-script
# - Implement universal version checking for commonFunctions.sh
#   ~ Recommend when cF.sh should be updated
#   ~ Log message if 'required' versions are mismatched
# - Change indexFolder to use 'du' or 'find $folder -type f'
#
# v2.0.7, 18 Feb. 2021, 20:57 PST

### Variables now stored in commonVars.sh

if [[ -n $cfVar ]]; then # This is an #IFNDEF to prevent multiple sourcing
    return
else
	export cfVar=0 # This needs to be at the beginning or it causes an infinite loop
fi

### Functions

## announce()
# Function: Make a visible notice to display text - catch the user's eye
# PreReq: None
#
# Call: announce [no-pause] <text1> [text2] [text3] ...
#
# Input: Text in quotation marks. Each argument will be a new line in the announcement.
#
# Output: None, no return values
#
# Other info: Try not to make text too long, it may not display correctly. Includes a check in case no arguments given.
#             If 'no-pause' is present, function will ignore default pause value
function announce() {
	# Determine highest amount of chars
	if [[ -z $1 ]]; then
		debug "l3" "No arguments given to announce()! Please fix, attempting to continue..."
		return 1
	fi
	
	# Check if pause should be skipped
	if [[ "$1" == no-pause ]]; then
		debug "l0" "Turning off pause for announce()..."
		noPause=0
		shift
	else
		noPause=1
	fi

	# If verbose mode is on, do not print stars, just the messages with "Announce: " appended to is
	# I really wanted this to be used with 'set -x', but I have no way to tell if it is set beforehand or not
	# 'set -x' is almost always used in conjunction with --verbose though, so it should be fine
	if [[ "$debugLevel" -lt 2 ]]; then
		printf "\n"
		
		for message in "$@";
		do
			printf "%bAnnounce:%b %b%b%b\n" "$announceCharColor" "$NC" "$announceTextColor" "$message" "$NC"
		done
		printf "\n"
		
		if [[ $noPause -eq 1 ]]; then
			sleep "$announceTimeout"s
		fi
		return 0
	fi
	
	# For everyting below, you can ignore SC2034. Too much work to change
	local stars=0
	for j in "$@"; # Stupid quotation marks killed the whole thing.... ugh....
	do
		if [[ ${#j} -gt $stars ]]; then # Finds the longest message to determine num of 'stars'
			stars=${#j}
		fi
	done
	((stars+=8)) # 4 beginning characters and 4 trailing
	
	# Set aChar to the default if it was not set in script
	if [[ -z $aChar ]]; then
		local aChar="$announceChar"
	fi
	local aChar="$(echo "$aChar" | head -c 1)" # Makes is so only the first char (byte) is used
	local threeChar="$aChar""$aChar""$aChar"
	
	# Make the beginning and end string, then print it once to begin
	local tmpStar=0
	local longString=""
	local spaceString="" # Clear these in case they were already set
	until [[ $tmpStar -eq $stars ]];
	do
		longString+="$aChar"
		if [[ $((tmpStar + 6)) -lt $stars ]]; then
			spaceString+=" "
		fi
		((tmpStar++))
	done
	printf "\n %b%b%b" "$announceCharColor" "$longString" "$NC"
	
	# Now, print announcements
	for i in $(seq 1 $#);
	do
		# First block prints the stars and spaces between statements
		printf "\n %b%b%b%b\n %b%b" "$announceCharColor" "$threeChar" "$spaceString" "$threeChar" "$threeChar" "$NC"
		
		# Math block to find out spaces for centering, for both even and odd numbers
		statement="${!i}"
		x=$((stars-${#statement}-6))
		if [[ $((x%2)) -eq 0 ]]; then
			evenFlag=1
		else
			evenFlag=0
		fi
		
		# Now print stars and statement, centering with spaces, depending on if even or odd
		case $evenFlag in
			1)
			for p in $(seq 1 "$((x/2))");
			do
				printf " "
			done
			printf "%b%b%b" "$announceTextColor" "${!i}" "$NC"
			for r in $(seq 1 "$((x/2))");
			do
				printf " "
			done
			;;
			0)
			for a in $(seq 1 "$((x/2))");
			do
				printf " "
			done
			printf "%b%b%b" "$announceTextColor" "${!i}" "$NC"
			for b in $(seq 1 "$((x/2+1))");
			do
				printf " "
			done
			;;
		esac
		printf "%b%b%b" "$announceCharColor" "$threeChar" "$NC"
	done
	
	# One last line of spaces
	printf "\n %b%b%b%b%b" "$announceCharColor" "$threeChar" "$spaceString" "$threeChar" "$NC"
	
	#Finally, print ending stars
	printf "\n %b%b%b \n\n" "$announceCharColor" "$longString" "$NC"
	
	# Sleep for specified time, then return
	if [[ $noPause -eq 1 ]]; then
		sleep "$announceTimeout"s
	fi
	return 0
}

## debug()
# Function: When enabled, it allows you to send debug messages to a log or stdout
#
# Call: debug [level] <message>
#
# Input: Level as described below. If not present, function will assume default level based on verbosity ($debugLevel)
#        Anything printed beyond the level will be put into a single printf statement ($@)
#
# Output: All messages print only to stderr. Returns 0 for success, 1 for errors.
#
# Other info: Function will only print debug messages that are -ge $debugLevel. Default is 2, so only WARN and above will print. 
#             Levels: 0 - DBG   (debug messages, NOTE these do not print to log!)
#                     1 - INFO  (general statements, progress)
#                     2 - WARN  (potential problems)
#                     3 - ERROR (function is broken, but script can still continue)
#                     4 - FATAL (script cannot continue and is stopping, missing args, etc.)
#             Adding -v|--verbose to a script will make debugLevel=1, -vv|--vverbose makes debugLevel=0
function debug() {
	# Sanity check
	if [[ -z $1 ]]; then
		printf "ERROR: No arguments given to debug()!\n" >&2
		return 1
	fi

	# Run init first time script calls debug()
	if [[ $debugInit -eq 0 ]]; then
		# First, determine output name. $longName if present, otherwise use cut on $0
		if [[ ! -z "$longName" ]]; then
			export logFile="$debugPrefix/$longName.log"
		else
			# Cuts filename from path if present, and removes extension if present
			export logFile="$debugPrefix/$(printf "$0" | rev | cut -d'/' -f1 | rev | cut -d'.' -f1).log"
		fi

		# Make sure $debugPrefix exists and is accessible
		if [[ ! -d "$debugPrefix" ]]; then
			if ! mkdir -p "$debugPrefix"; then
				printf "FATAL: Unable to create debugPrefix %s as $(whoami), make sure user has permissions!\n" "$debugPrefix" >&2
				return 1
			fi
		elif ! touch "$logFile"; then
			printf "FATAL: Cannot use debugPrefix %s as user $(whoami), make sure user has permissions!\n" "$debugPrefix" >&2
			return 1
		fi
		chmod 777 "$logFile" # This way anyone trying to write to it won't have errors
		
		# Put the intro line into the log
		local startTime="$(date)"
		printf "\n*** Started at %s ***\n" "$startTime" | tee -a "$logFile" 1>/dev/null # Will change to redirection if I ever see a system without coreutils installed lol

		# Finally, set the seconds the first debug() was called
		export startSeconds=$SECONDS
		export debugInit=1
	fi

	# Continue with function. Set current seconds and blurb, if present
	local currSeconds=$SECONDS
	if [[ ! -z $shortName ]]; then
		local prBlurb="$debugNameColor""$shortName""$NC"": "
	fi
	
	# Turn time into hours, minutes, and days
	local prSeconds=$currSeconds
	local prMinutes=0
	local prHours=0
	local prDays=0
	local prString=""
	while [[ $prSeconds -ge 60 ]]; do
		((prSeconds-=60))
		((prMinutes+=1))
	done
	while [[ $prMinutes -ge 60 ]]; do
		((prMinutes-=60))
		((prHours+=1))
	done
	while [[ $prHours -ge 24 ]]; do
		((prDays+=1))
		((prHours-=24))
	done
	
	# Add seconds and minutes to script. Only add hours and days if script runs for that long to compact space
	if [[ $prDays -gt 0 ]]; then
		if [[ $prDays -lt 10 ]]; then
			prString="$prString""0"
		fi
		prString="$prString""$prHours:"
	fi

	if [[ $prHours -gt 0 ]]; then
		if [[ $prHours -lt 10 ]]; then
			prString="$prString""0" # Add a zero at the start to look cleaner
		fi
		prString="$prString""$prHours:"
	fi
	
	if [[ $prMinutes -lt 10 ]]; then
		prString="$prString""0" # Add a zero at the start to look cleaner
	fi
		prString="$prString""$prMinutes:"

	if [[ $prSeconds -lt 10 ]]; then
		prString="$prString""0" # Add a zero at the start to look cleaner
	fi
	prString="$prString""$prSeconds"
	
	# Set the local variables for easy output later
	local prLevel=$debugLevel
	local prLevelName="WARN"
	local prColor="$debugWarnColor"
	# Set the output level, if given, as well as the corresponding string
	if [[ "$1" == l* ]]; then
		case $1 in
			l5|l0)
			prLevel=0
			prLevelName="DBG"
			prColor="$debugDbgColor"
			;;
			l1)
			prLevel=1
			prLevelName="INFO"
			prColor="$debugInfoColor"
			;;
			l2)
			prLevel=2
			prLevelName="WARN" # Because always be explicit!
			prColor="$debugWarnColor"
			;;
			l3)
			prLevel=3
			prLevelName="ERROR"
			prColor="$debugErrorColor"
			;;
			l4)
			prLevel=4
			prLevelName="FATAL"
			prColor="$debugFatalColor"
			;;
			*)
			printf "ERROR: Unknown level %s given to debug()! Attempting to continue..." "$1" >&2
			;;
		esac
		shift # So that the level doesn't get included in the debug message
	fi
	
	# Output string to console needed
	if [[ $prLevel -ge $debugLevel ]]; then
		printf "%b[ %b ]%b %b%b%b: %b %b\n" "$debugTimeColor" "$prString" "$NC" "$prBlurb" "$prColor" "$prLevelName" "$*" "$NC" >&2 
	fi
	
	# And for my final trick, I will output the same string to the log file but without the color text!
	if [[ $prLevel -ne 0 ]]; then # Except in DBG mode
		printf "[ %s ] %s: %s\n" "$prString" "$prLevelName" "$*" | tee -a "$logFile" 1>/dev/null
	fi
	return 0
}

## addCronJob()
# Function: Like the name implies, creates a cron job for the current user
#
# Call: addCronJob <number_of_mins> <min|hour> "/path/to/command.sh -in quotations" [no-null]
#
# Input: Number of minutes or hours to run script, min or hour indicator, and the command in quotation marks
#
# Output: stdout, returns a 1 if it could not be added, 0 if successful
#
# Other info: First three variables require in the correct order. Second variable accepts (min, mins, minutes, minute, hour, hours).
#             If the 4th argument is present, it will NOT redirect stdout to /dev/null. Otherwise cron will send you mail.
#             Only hours and minutes for now, might add more later. In addition, find a way to specify '15' vs '*/15'.
function addCronJob() {
	# First, determine if time is valid
	if [[ $1 -le 0 || $1 -gt 60 ]]; then
		debug "l3" "Call for addCronJob() is outside the natural time limits! (Num: $1)" # Don't you just LOVE cryptic messages?
		return 1
	elif [[ $2 == "hour" && $1 -gt 24 || $2 == "hours" && $1 -gt 24 ]]; then
		debug "l3" "There are not $1 hours in a day, please fix call!"
		return 1
	fi
	
	case $2 in
		min|mins|minute|minutes)
		announce "Preparing job $3 for cron!" "Job will run every $1 minutes from now on."
		
		if [[ ! -z $4 ]]; then
			debug "l2" "cronjob $3 will NOT redirect to /dev/null, expect lots of mail from cron!"
			touch tmpCron # Wasn't going to include this at first, but just in case user doesn't have write permission...
			crontab -l 2>/dev/null > tmpCron
			printf "\n# Added by %s on %s\n*/%s * * * * %s\n" "$0" "$(date)" "$1" "$3" >> tmpCron
			crontab tmpCron
			rm tmpCron
		else
			touch tmpCron # Wasn't going to include this at first, but just in case user doesn't have write permission...
			crontab -l 2>/dev/null > tmpCron
			printf "\n# Added by %s on %s\n*/%s * * * * %s &>/dev/null\n" "$0" "$(date)" "$1" "$3" >> tmpCron
			crontab tmpCron
			rm tmpCron
		fi
		;;
		hour|hours)
		announce "Preparing job $3 for cron!" "Job will run once per day at $1 o'clock, Military Time"
		
		if [[ ! -z $4 ]]; then
			debug "l2" "cronjob will NOT redirect to /dev/null, expect lots of mail from cron!"
			touch tmpCron # Wasn't going to include this at first, but just in case user doesn't have write permission...
			crontab -l 2>/dev/null > tmpCron
			printf "\n# Added by %s on %s\n* %s * * * %s\n" "$0" "$(date)" "$1" "$3" >> tmpCron
			crontab tmpCron
			rm tmpCron
		else
			touch tmpCron # Wasn't going to include this at first, but just in case user doesn't have write permission...
			crontab -l 2>/dev/null > tmpCron
			printf "\n# Added by %s on %s\n* %s * * * %s &/dev/null\n" "$0" "$(date)" "$1" "$3" >> tmpCron
			crontab tmpCron
			rm tmpCron
		fi
		;;
		*)
		debug "l2" "ERROR: Unknown call for addCronJob()! Value: $2"
		return 1
		;;
	esac
	
	return 0
}

## getUserAnswer()
#
# Function: Asks a user for input, verifies input, then returns with the answer (0 for true/yes, 1 for false/no)
#
# Call: getUserAnswer [y/n] "Question in quotation marks?" [variable_name] "Question for variable name, if present?"
#
# Input: User will be asked the question in quotes ($1). 
#        If [variable_name] ($2) is present, it will ask the second question ($3) and assign response to that variable
#
# Output: stdout (obviously), return value of 0 for yes/true response, value of 1 for no/false response
#
# Other info: Be careful which names you give to the variables, you may accidentally delete other variables!
#             If first argument is y or n, it will assume answer is yes/no respectively. Script will not assume input values (for now)
# TODO: Ask user for boolean, integer, or choice, then verify
#       Possibly using whiptail?
function getUserAnswer() {
	local ans="NULL" # Guess re-declaration doesn't work properly in bash...
	local assume="nothing"

	# Allows for assuming yes/no without needed to edit all current calls
	if [[ "$1" == "y" || "$1" == "yes" || "$1" == "true" ]]; then
		assume="yes"
		shift
	elif [[ "$1" == "n" || "$1" == "no" || "$1" == "false" ]]; then
		assume="no"
		shift
	fi
	
	# First, give question to the user
	announce "$1"
	
	# Then, get answer from user in form of binary
	until isBoolean "$ans"; do
		printf "%bPlease answer above prompt %b" "$getUserAnswerPromptColor" "$NC"
		if [[ "$assume" == "yes" ]]; then
			printf "%b(%bY%b/n)%b: " "$getUserAnswerAssumeColor" "$RED" "$getUserAnswerAssumeColor" "$NC"
		elif [[ "$assume" == "no" ]]; then
			printf "%b(y/%bN%b)%b: " "$getUserAnswerAssumeColor" "$RED" "$getUserAnswerAssumeColor" "$NC"
		else
			printf "%b(y/n)%b: " "$getUserAnswerAssumeColor" "$NC"
		fi

		if ! read -t "$getUserAnswerTimeout" ans; then
			printf "\n" # Formatting
			ans="$assume"
		fi
	done

	# If user needs a variable set, do that now
	if [[ -n $2 ]] && yesorno "$ans"; then
		if [[ -z $3 ]]; then
			debug "l2" "Incorrect call for function getUserAnswer()! Please look at documentation!"
		else
			announce "$3"
			printf "%bPlease assign a value to %b%b%b: " "$getUserAnswerPromptColor" "$getUserAnswerAssumeColor" "$2" "$NC"
			read ${2}
		fi
	fi
	
	# Finally, return binary value. Implicit false, explicit yes
	if yesorno "$ans"; then
		return 0
	else
		return 1
	fi
}

## pause()
#
# Function: Prompts the user to press Enter to continue the script (or any message)
#
# Call: pause "prompt"
#
# Input: By including a prompt as $1, it will display that (make sure to tell user to press [Enter]!)
#
# Output: stdout. Returns the return value from 'read'.
#
# Other info: If $1 is missing, it will use default prompt ot "Press [Enter] to continue..."
function pause() {
	debug "l5" "pause() has been called" # Let's verbose users know what is happening
	if [[ -z $1 ]]; then
		printf "%bPress [Enter] to continue...%b\n" "$pauseTextColor" "$NC"
	else
		printf "%b%b%b\n" "$*" "$pauseTextColor" "$NC"
	fi
	read -p ""
	return $? # Why not
}

## editTextFile()
#
# Function: Let the user edit a text file, then return to the script
#
# Call: editTextFile <text_file>
#
# Input: A text file to be edited
#
# Output: Opens editor with the provided file. Some stdout. Returns exit code from editor
#
# Other: Defaults to nano. If user is over 40 years old, it will use vi instead (lol). Uses $EDITOR and $VISUAL first.
function editTextFile() {
	# First, check if file was provided. Return error if not.
	if [[ -z $1 ]]; then
		debug "l3" "Incorrect call for editTextFile(), please consult commonFunctions.sh! Script will continue anyways, press CTRL+C to quit!"
		return 1
	fi
	
	# Now, find the editor and run it
	if [[ -z $EDITOR && -z $VISUAL ]]; then
		if [[ -z $(which nano 2>/dev/null) ]]; then
			debug "l2" "User error has lead to vi being the only editor, using it as a last resort!"
			# Was gonna delete this when I introduced new debug() format, but this is too good to delete lol
			announce "It seems vi is your only editor. Strange choice, or new installation?" "When done editing, press :wq to exit vi"
			vi "$1"
		else
			debug "l1" "Letting user edit $1 with nano"
			nano "$1"
		fi
	elif [[ -z $EDITOR ]]; then
		debug "l1" "Letting user edit $1 with $VISUAL"
		eval "$VISUAL" "$1"
	else
		debug "l1" "Letting user edit $1 with $EDITOR"
		eval "$EDITOR" "$1"
	fi
	return $?
}

## win2UnixPath()
#
# Function: Converts a Windows path to a POSIX path and echoes the response to stdout; typical use case will look like the following:
#           directory="$(win2UnixPath "$windowsDirectory")"
#
# Call: win2UnixPath <Windows_path> [prefix] [upper OR cut] [space]
#
# Input: String containing a Windows path
#
# Output: Outputs converted string to stdout
#
# Other: By default, the Windows 'root' drive (C:\) will be converted tolower, useful in Bash for Windows (make sure prefix="/mnt" in this case!)
#        Appending upper to the end of the call will leave the uppercase letter intact; appending cut will remove the Windows root outright
#        Both of these appendages can be used by themselves, but the prefix must ALWAYS go first if it is not set somewhere else in the script!
#        As of 1.9.1, function will not automatically convert spaces. Include "space" at the end of the call to have this done from now on.
function win2UnixPath() {
	# Make sure an argument is given
	if [[ -z $1 ]]; then
		debug "l3" "No argument given for win2UnixPath()! Attempting to continue..."
		return 1
	fi
	
	local dir="$1" # Getting ready to have nasty things done to it
	
	# Explained: winDir            \ -> /       : -> ''    ' ' -> '\ '
	dir="$(echo "/$dir" | sed -e 's/\\/\//g' -e 's/://')" #-e 's/ /\\ /g')"
	
	# Determing if second argument is prefix
	if [[ $# -gt 2 ]]; then
		prefix="$2" # Assume the user knows what they're doing
		shift
	elif [[ -d "$2" ]]; then
		prefix="$2"
		shift
	fi
	
	# Now, do 'cut', 'upper', or 'space' if the user requested it
	if [[ ! -z $2 ]]; then
		case "$2" in
			u*) # upper in documentation
			true # Essentiallly, do nothing, since default behavior is to make 'c drive' lowercase (built for bash on Windows!)
			;;
			c*) # cut in documentation
			dir="$(echo "$dir" | cut -d'/' -f2 --complement)"
			#dir="/""$dir" # Command cut off root in trials, this can be remedied later anyways
			;;
			s*)
			dir="$(echo "$dir" | sed -e 's/ /\ /g')"
			;;
			*)
			debug "l3" "Bad call for winToUnixPath(): $2 is neither an acceptable command nor a valid prefix!"
			return 1
			;;
		esac
		shift
	else
		# Convert the Windows 'root' drive tolower, common use in Bash for Windows
		drive="$(echo "$dir" | cut -d'/' -f2 | awk '{print tolower($1)}')"
		dir=/"$drive""$(echo "$dir" | cut -d'/' -f2 --complement)" # Scary, only way to test this is to run the script!
	fi
	
	# Now that THAT'S all over with, time to add the prefix!
	if [[ ! -z $prefix ]]; then
		dir="$prefix"/"$dir"
	fi
	
	# Change any escape characters
	#                             $ -> \$       @ -> \@       # -> \#       ! -> \!
	#dir="$(echo "$dir" | sed -e 's,\$,\\\$,g' -e 's,\@,\\\@,g' -e 's,\#,\\\#,g' -e 's,\!,\\\!,g')"
	
	# Lastly, if the user requested it, edit the spaces; no need to check contents of last call, should be the only argument
	[[ ! -z $2 ]] && dir="$(echo "$dir" | sed -e 's/ /\\ /g')"
	
	# One final cleanup... Change any double slashes to single
	dir="$(echo "$dir" | sed -e 's,//,/,g' -e 's,\\\\,\\,g')"
	
	# Congratulations if you made it this far!
	echo "$dir"
	return 0
}

## checkout()
#
# Function: Used for parallel processing. Makes it so only one instance of the specified function can be run at a time, based on the lock variable given
#           Once function is called, it will wait until the specified function is available, waiting random times in ms. Then, it will return so script can continue.
#           It will lock the variable beore returning so no one else can use the function. Make sure to run 'checkout done <var>' when the function is done!
#
# Call: checkout <wait|done> <lockVarName>
#
# Input: Variable you wish to use as the lock for the process
#
# Output: Stderr, if any errors are encountered. Doesn't call debug (minus at beginning) because it could cause infinite loops.
#
# Other: To checkout the function for use, run 'checkout wait <lockVar>'. Be sure to include 'checkout done <lockVar>' when you are done though!
function checkout() {
	if [[ $# -ne 2 ]]; then
		# I know I said not to call debug, but I know how this works and what I'm doing. No infinite loops because I do everything correctly (r/IAmVerySmart)
		debug "l3" "Incorrect number of arguments for checkout()! Please read documentation and try again!"
		return 1
	fi
	
	# Make sure $lpDir directory exists, where the lock will be placed
	if [[ ! -d "$lpDir" ]]; then
		debug "l2" "$lpDir directory has not been created! Doing so now..."
		if ! mkdir -pv "$lpDir"; then # Using this directory shouldn't be an issue, as scripts using this will likely be run by the same user
			debug "l3" "Cannot create directories for $lpDir! Does $(whoami) have permission? Continuing..."
			return 1
		fi
	fi
	local lockVar="$HOME"/"$2".lock # How did I not think of doing this when I created this function? local is a lifesaver!
	
	# Assuming the correct number of variables...
	case $1 in
		w*)
		# First, wait 100-300ms before moving on, in case another process got here first
		echo -e "import time\nimport random\ntime.sleep(random.uniform(0.1,0.3))\nexit()" | python
		
		# Then, claim your variable
		if [[ ! -e "$2"".lock" ]]; then
			touch "$lockVar"
			return 0
		fi
		
		# Use this loop if file is locked. Waits in intervals of 50-100ms
		until [[ ! -e "$lockVar" ]];
		do
			echo -e "import time\nimport random\ntime.sleep(random.uniform(0.05,0.1))\nexit()" | python # This sleeps for a random time between 50-100ms
		done
		
		# Wait one last time, just in case
		echo -e "import time\nimport random\ntime.sleep(random.uniform(0.1,0.3))\nexit()" | python
		touch "$lockVar"
		return 0
		;;
		d*)
		rm "$lockVar" # Lock deleted
		if [[ "$?" -ne 0 ]]; then # Notify user, might be a slow system
			debug "l3" "$2.lock was missing! Did another process step over it? Attempting to continue..."
			return 1
		fi
		return 0
		;;
		*)
		(>2& printf "FATAL: %s is an incorrect option for checkout()! Please read documentation and retry!" "$1")
		return 1
		;;
	esac
	return 1 # Technically it failed if it made it this far
}

## importText()
#
# Function: Import the given text (or other newline-delimited file) to the given variable
#           I use this so often I finally decided to make it a common function
#
# Call: importText <filename> <variable> [include_hash]
#
# Input: filename of the text file, variable where the imported file will be stored
#        If the last var is present, function will put ALL lines into the variable, instead of ignoring '#' comments (default)
#
# Output: Stderr for problems, and an ARRAY with the text file contents
#         Return value of 0 on success, value of 1 if there was a problem. Let's the script decide whether or not to quit
#
# Other: NOTE - everything is stored in an array, so when examining it in a loop, make sure you use "$var[@]"!
function importText() {
	# argc check
	if [[ -z $2 ]]; then
		debug "l4" "Incorrect call for importText()!"
		return 1
	fi
	
	# Make sure we can see the file
	if [[ -f "$1" ]]; then
		local fileName="$1"
	else
		debug "l3" "$1 is not a file!"
		return 1
	fi
	local var="$2"
	#declare -a "${var}"
	if [[ ! -z $3 ]]; then
		debug "l2" "importText() will import comments as well!"
		local comments=1
	fi
	
	# Now, read the file to the variable
	local count=0
	while read -r line
	do
		[[ "$line" == "" || "$line" == " " ]] && continue # Skip blank and empty lines, everytime
		[[ "$line" == \#* && -z $comments ]] && continue # Conditionally skip comments
		
		 mapfile -t -O "$count" "${var}" <<< "$line"
		((count++))
	done < "${fileName}"
	debug "l5" "Read $count lines into variable $var!"
	return 0
}

## dynamicLinker()
#
# Function: Dynamically link script to given directory, /usr/bin, or first folder in $PATH
#           Location will be determined in the above order
#
# Call: dynamicLinker <script_location> [PATH_location]
#
# Input: Script (if not absolute path, will be converted), location to send links
#
# Output: Stderr if problems occur, otherwise nothing besides log
#
# Other: Works on all files. Also, NOTE: all links will be symbolic!
function dynamicLinker() {
	if [[ -z $1 ]]; then
		debug "l3" "No arguments given with dynamicLinker()! Please read documentation and try again!"
		return 1
	fi
	
	# Check if path is absolute
	local fullScriptPath=""
	if [[ "$1" == \/* ]]; then
		debug "l1" "Argument given to dynamicLinker ($1) is absolute, moving on..."
		fullScriptPath="$1"
	elif [[ -z $(ls *"$1"* 2>/dev/null) ]]; then
		debug "l2" "Script $1 is not in current folder, attempting to determine absolute path..."
		# The following is brought to you by none other than StackOverflow!
		# https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
		SOURCE="${BASH_SOURCE[0]}"
		while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
		  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
		  SOURCE="$(readlink "$SOURCE")"
		  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
		done
		fullScriptPath="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
	else
		debug "l2" "Path is not absolute, but script is present in directory. Combining and moving on..."
		fullScriptPath="$(pwd)"/"$1"
	fi
	
	if [[ -z $fullScriptPath ]]; then
		debug "l4" "Script location could not be determined! Please manually link scripts!"
		return 1
	fi
	
	# Absolute path set, now determine which path to link to
	if [[ ! -z $2 ]]; then
		if [[ ! -d "$2" || -z $(echo "$PATH" | grep "$2") ]]; then
			debug "l3" "Given argument $2 is not a directory, or is not in user's path! Please fix and re-run!"
			return 1
		fi
		debug "l1" "Given argument $2 is in user's path! Moving on..."
		linkLocation="$2"
	elif [[ ! -z $(echo "$PATH" | grep /usr/bin) ]]; then
		debug "l1" "No PATH given, assuming /usr/bin. Continuing..."
		linkLocation="/usr/bin"
	else
		# No PATH given and /usr/bin somehow not in user's path. Use first directory from PATH instead
		linkLocation="$(echo "$PATH" | cut -d':' -f1)"
		debug "l2" "No path given to dynamicLinker(), and /usr/bin not in user's PATH! Resorting to first PATH instead, $linkLocation..."
	fi
	
	if [[ "$linkLocation" != \/* ]]; then
		debug "l4" "Path ($linkLocation) is not absolute! Please link manually!"
		return 1
	fi
	
	# Finally, figure out the how many links to do
	numLinks=1 # Full script name
	if [[ ! -z $(cat "$fullScriptPath" | grep longName=) ]]; then
		((numLinks+=2))
	fi
	if [[ ! -z $(cat "$fullScriptPath" | grep shortName=) ]]; then
		((numLinks+=4))
	fi
	
	# And now, for our grand finale, watch as we link everything together!
	debug "l2" "Linking requires sudo privileges, please provide when asked!"
	touch "$lpDir"/.linkList # Place to store links so they can be removed later
	local linkName=""
	while [[ $numLinks -gt 0 ]]; do
		case $numLinks in
		1)
			linkName="$linkLocation"/"$(echo "$fullScriptPath" | rev | cut -d'/' -f1 | rev)"
			((numLinks-=1))
			;;
		3)
			#linkName="$(echo "$fullScriptLocation" | rev | cut -d'/' -f1 --complement | rev)"
			linkName="$linkLocation"/"$(cat "$fullScriptPath" | grep longName= | cut -d'=' -f2 | sed -e 's/\"//g')"
			((numLinks-=2))
			;;
		[57])
			#linkName="$(echo "$fullScriptLocation" | rev | cut -d'/' -f1 --complement | rev)"
			linkName="$linkLocation"/"$(cat "$fullScriptPath" | grep shortName= | cut -d'=' -f2 | sed -e 's/\"//g')"
			((numLinks-=4))
			;;
		*)
			debug "l3" "Unexpected case number in dynamicLinker()! Returning, please link manually..."
			return 1
			;;
		esac
		if [[ -e "$linkName" ]]; then
			debug "l3" "Link/file at $linkName already exists, skipping and continuing!"
		else
			debug "l1" "Attempting to link $fullScriptPath to $linkName"
			sudo ln -s "$fullScriptPath" "$linkName"
			val="$?" # Done this way to report error code
			if [[ $val -ne 0 ]]; then
				debug "l2" "An error occurred while attempting to link $fullScriptPath to $linkName! Error code: $val"
			fi
			echo "$linkName" >> "$lpDir"/.linkList
		fi
	done
	
	# Done with function
	return 0
}

## myConfig()
#
# Function: Import and manage a config file for a script, based off the name from $debugOutputPrefix
#           To be used on scripts that run the same setting everytime, to avoid setup
#           NOTE: Scripts must be written in a way to take advantage of this!
#           Filename is in case of multiple scripts using a global config file.
#
# Call: myConfig [file] <import/export>           - Imports or exports the config to be used next time; filename optional, can be shortname or full path. 
#                       <add> <var>               - Adds the var to the config in the form of "var=$value"; Deletes previous value if present
#                       <del> <var>               - Deletes the var from the config file, if it exists
#                       <remove>                  - Deletes both the temp and saved config. (Saved config gets moved to a .old file, just in case though)
#                       <prep>                    - Preps the files to be used later. Functionally, doesn't do anything except make sure temp config is ready.
#                       <comment>                 - Adds a comment, prefixed with a '#' to the file
#                       <show> [saved]            - Shows the contents of the config file being used. Shows temp by default, add a second arg to see saved config.
#                       <secure>                  - Secure file so only $USER can access it
#
# Input: Required args
#
# Output: Stderr if errors are found, and stdout for <show>
#         Returns 0 for success, non-zero for error. Error reporting recommended on script-level
#
# Other: Remember to add and export and import statement where needed in your scripts!
#        Output filename will be $lpDir/"$shortName".conf. Directories and files will be made as needed.
function myConfig() {
	# First, make sure args were given
	if [[ -z $1 ]]; then
		debug "l3" "No arguments given to myConfig()! Please read documentation and try again!"
		return 1
	fi
	
	# Now, make sure directory exists
	if [[ ! -d "$lpDir" ]]; then
		debug "l2" "Recursively creating config directory $lpDir!"
		if ! mkdir -pv "$lpDir"; then # Hopefully I don't regret making this recursive...
			debug "l3" "Unable to create directories for $lpDir! Does $(whoami) have permission?"
			return 1
		fi
	fi
	
	# Sanity check - if prefix is set to "Debug" or is empty, tell user to fix script or set shortName
	if [[ -z $shortName ]]; then
		debug "l4" "Output prefix is not set, cannot continue with myConfig()!"
		announce "Please fix your script to use a shortName" "To do this, add the line \"export shortName=<name>\" before sourcing commonFunctions.sh!"
		return 1 # Debated putting an "exit 1" here, but that felt a little extreme
	fi
	
	# Check to see if global config is being used
	# Using level 5 debugging below because there's no need for every report of this in scripts that actually use this functionality
	local configFile=""
	if [[ "$1" == *.* ]]; then
		debug "l5" "Using global config $1 for myConfig()!"
		if [[ "$1" == /* && -f "$1" ]]; then
			debug "l5" "File given is full path to a file! Using for global config..."
		elif [[ "$1" == */* ]]; then # Subdirectories of .lpconf not supported at this time. Fix your program, you naughty coder!
			debug "l3" "Global config path $1 is not a full path! Exiting..."
			return 1
		else
			debug "l5" "Global config $1 appears to be a relative path. Checking pwd."
			if [[ -f "$1" ]]; then
				debug "l5" "Using pwd as global config location!"
				configFile="$(pwd)"/"$1".conf
			else
				debug "l5" "Not in present directory, assuming normal linux-pref directory!"
				configFile="$lpDir"/"$1".conf
			fi
		fi
		shift # Very important step. Not that it would be THAT hard to correct. Work smarter, not harder.
		# It SHOULDN'T happen, but saving configs could overwrite each other using global config! If this becomes a problem, figure out how to fix it with checkout()!
	else
		debug "l5" "DBG: Using normal path location for myConfig()!"
		configFile="$lpDir"/"$shortName".conf
	fi
	local tmpConfig="$configFile".tmp
	
	# Get rid of previous tmp config, if it exists, on first run only
	if [[ -z $mcFirstRun ]]; then
		debug "l1" "Running myConfig() for the first time this run..."
		if [[ -f "$tmpConfig" ]]; then
			debug "l2" "Possible loss of data, removing old temp config!"
			rm "$tmpConfig"
		fi
		export mcFirstRun=1
	fi
	
	# Now, copy over tmpConfig. Everything is done inside file, then copied back over when asked
	if [[ -f "$configFile" && ! -f "$tmpConfig"	]]; then # Second statement prevents re-copying, and possible loss of data
		debug "l1" "Config file found, copying to $tmpConfig to work on"
		cp "$configFile" "$tmpConfig"
	elif [[ ! -f "$tmpConfig" ]]; then # Figured out why this message kept popping up... elif>else
		debug "l2" "No config file found, creating a temp one!"
		touch "$tmpConfig" # Prevents errors from tee later
		chmod 0777 "$tmpConfig" # In case it gets put into a global location
	fi
	
	# Now, for all the heavy lifting
	case $1 in
		imp*)
		if [[ ! -f "$configFile" ]]; then
			debug "l3" "No config file found at $configFile! Has it been created yet?"
			return 1
		else
			debug "l1" "$configFile as been found, attempting to read it"
			source "$configFile"
			return "$?" # Safest thing to do at that point
		fi
		;;
		exp*)
		if [[ ! -f "$tmpConfig" ]]; then
			debug "l4" "No config file found to export for myConfig()! Please read documentation and try again!"
			return 1
		fi
		debug "l1" "Deleting original config and saving temp one!"
		rm "$configFile"
		if ! cp -f "$tmpConfig" "$configFile"; then # Assume user knew what they were doing, they're own fault if not tbh
			debug "l2" "Could not copy temp config. assuming sudo is needed..."
			if ! sudo cp -f "$tmpConfig" "$configFile"; then
				debug "l3" "Could not link $tmpConfig to $configFile! Does user have permissions? Returning!"
				return 1
			else
				debug "l1" "Copy successful with sudo, changing owner and permissions."
				if ! sudo chown "$USER:$USER" "$configFile"; then
					debug "l3" "Could not change ownership of $configFile to $USER! Please fix manually!"
				fi
				if ! sudo chmod 0777 "$configFile"; then
					debug "l3" "Could not change permissions of $configFile! Please fix manually!"
				fi
			fi
		fi
		return "$?"
		;;
		com*)
		if [[ -z "$2" ]]; then
			debug "l3" "No comment given to add to config for myConfig()! Please read documentation and try again!"
			return 1
		fi
		debug "l5" "Adding line $2 to temp config!"
		printf "# %s\n" "$2" | tee -a "$tmpConfig" 1>/dev/null
		return 0
		;;
		rem*)
		debug "l2" "User has indicated to remove config! Moving saved to .old and removing temp..."
		if [[ -f "$tmpConfig" ]]; then
			rm "$tmpConfig"
		else
			debug "l5" "$tmpConfig somehow did not exist, unable to remove..."
		fi
		
		if [[ -f "$configFile" ]]; then
			if [[ -f "$configFile".old ]]; then
				debug "l5" "Removing old .old config!"
				rm "$configFile.old"
			fi
			mv "$configFile" "$configFile".old
		else
			debug "l2" "No saved config found, nothing to do!"
		fi
		touch "$tmpConfig"
		return 0
		;;
		add|edit)
		if [[ -z $2 ]]; then
			debug "l3" "Not enough args given for myConfig()! Please read documentation and try again!"
			return 1
		fi
		# BE WARNED! This will delete anything matching pattern! e.g. myConf, myConfLocation would both be deleted. Be unique with saved vars!
		sed -i "/$2/ { N; d;}" "$tmpConfig" # Deletes line where this var is located. https://unix.stackexchange.com/a/152316 because credit
		debug "l2" "Adding line $2=${!2} to the temp config!"
		printf "export %s=\"%s\"\n\n" "$2" "${!2}" | tee -a "$tmpConfig" 1>/dev/null # Again, assume this is what the user actually meant
		return "$?"
		;;
		del|rm)
		if [[ -z $2 ]]; then
			debug "l4" "No argument given to delete for myConfig()! Please read documentation and try again!"
			return 1
		else
			debug "l2" "Attempting to delete line $(sed -e '/$2/ { p; }' "$tmpConfig") from the temp config!" # Starting to get clever
			sed -i "/$2/ { N; d;}" "$tmpConfig"
			return "$?"
		fi
		;;
		prep)
		debug "l2" "myConfig() is prepped and ready to be used!" # Prep is done earlier in the script, so this is fine
		;;
		secure)
		debug "l1" "Securing myConfig files!"
		if ! chmod 0700 "$tmpConfig"; then
			debug "l2" "Could not change permissions of tmp config $tmpConfig! Attempting to continue..."
		else
			debug "l1" "tmp config $tmpConfig is now secure"
		fi

		if ! chmod 0700 "$configFile"; then
			debug "l3" "Unable to change permissions of config file $configFile! Does it exist? Please fix manually!"
		else
			debug "l1" "Config file $configFile is no secure!"
		fi
		;;
		show|list)
		if [[ -z $2 ]]; then
			debug "l1" "Displaying contents of $tmpConfig"
			cat "$tmpConfig"
		else
			debug "l1" "INFO: Displaying saved config at user request"
			cat "$configFile"
		fi
		return "$?"
		;;
		*)
		debug "l4" "Unknown argument $1 given to myConfig()! Please read documentation and try again!"
		return 1
		;;
	esac
	return 0 # You never know
}

## newFileScan()
#
# Function: List new file(s) in given folder after expected changes
#           Meant to be used when a new file is expected, but you do not know the name of said file
#           NOTE: Only returns new files by default, not directories!
#           If no directory is given as first argument, function will assume pwd!
#
# Call: newFileScan [opt] [dir] <start/init>    - Initializes the directory for scanning, removes previous scan info if found
#                               <get/scan>      - Returns any new files since the last scan, then stores results
#                               <new>           - Returns new files without saving results
#                               <clean>         - Clean scan info, if present
#       Options:
#       -h | --hidden           - Include hidden files and folders
#       -f | --folders          - Include new folders in search results as well
#
# Input: See above
#
# Output: Prints new files to stdout, meant to be captured with `var="$(newFileScan ...)"`
#         stderr for any errors that occur
#         Returns 0 for sucess, 1 for error, though this will usually be ignored
#
# Other: Function will add up to three new files to the directory: .nfsInit, .nfsScan, and .nfsNew
#        These files will not be included in scan, even if the hidden flag is active
function newFileScan() {
	# Sanity check
	if [[ -z $1 ]]; then
		debug "l2" "ERROR: No arguments given to newFileScan()! Please read config and retry!"
		return 1
	fi

	# Process arguments
	local scanDir="$(pwd)"
	local scanMode="init"
	local scanHidden=1
	local scanFolder=1
	while [[ ! -z $1 ]]; do	
		if [[ -d "$1" ]]; then
			scanDir="$1"
			debug "l5" "Working folder for nfs() is now $scanDir"
		else
			case "$1" in
				start|init|begin)
				scanMode="init"
				debug "l5" "Scan mode for nfs() is now $scanMode"
				;;
				get|scan)
				scanMode="scan"
				debug "l5" "Scan mode for nfs() is now $scanMode"
				;;
				new)
				scanMode="new"
				debug "l5" "Scan mode for nfs() is now $scanMode"
				;;
				clean)
				debug "l1" "Cleaning scan info for $scanDir"
				# WARN: This assumes the correct directory already set! User beware!
				for file in {.nfsInit,.nfsNew,.nfsScan}; do
					if [[ -f "$scanDir/$file" ]]; then
						if ! rm "$scanDir/$file" 1>/dev/null; then
							debug "l2" "ERROR: Couldn't delete $file from $scanDir, do you have permission?"
						fi
					else
						debug "l2" "WARN: $file did not exist in $scanDir!"
					fi
				done
				return 0
				;;
				-h|--hidden)
				scanHidden=0
				debug "l1" "newFileScan() set to return hidden files!"
				;;
				-f|--fold*)
				scanFolder=0
				debug "l1" "newFileScan() will return new folders now!"
				;;
				*)
				debug "l3" "Unknown option $1 given to newFileScan()! Please fix and re-run!"
				return 1
				;;
			esac
		fi
		shift
	done

	# Ready to begin work
	# Begin with a scan since all the others will need it
	local fArgs="$scanDir -maxdepth 1" # https://serverfault.com/questions/368370/how-do-i-exclude-directories-when-listing-files/368396#368396
	if [[ "$scanFolder" == 1 ]]; then
		fArgs="$fArgs -not -type d"
	fi

	if [[ "$scanHidden" == 1 ]]; then
		fArgs="$fArgs -not -name '.*'"
	fi
	if ! eval find "$fArgs" | tee "$scanDir/.nfsNew" 1>/dev/null; then
		debug "l3" "Could not write to $scanDir, do you have permission? Please fix and re-run!"
		return 1
	fi

	# Now, decide how to do comparisons
	local rString=""
	if [[ "$scanMode" == "init" ]]; then
		if [[ -f "$scanDir/.nfsInit" ]]; then
			debug "l5" "Deleting previous init file $scanDir/.nfsInit"
			if ! rm "$scanDir/.nfsInit"; then 
				debug "l2" "Could not delete $scanDir/.nfsInit! Do you have permission?"
				return 1
			fi
			mv "$scanDir/.nfsNew" "$scanDir/.nfsInit"
		fi
		return 0 # Success if made it this far
	else
		if [[ ! -f "$scanDir/.nfsNew" ]]; then
			debug "l2" "$scanDir was not initialized, so no comparison could be done! Continuing..."
			mv "$scanDir/.nfsNew" "$scanDir/.nfsInit"
			return 1 # Technically an error
		else
			local f1="$scanDir/.nfsInit"
		fi

		if [[ -f "$scanDir/.nfsScan" ]]; then
			f1="$scanDir/.nfsScan" # Use scan, if present, to show changes upon changes, etc.
		fi
		rString="$(comm -1 -3 "$f1" "$scanDir/.nfsNew" | awk '!/.nfs/{print }')" # https://stackoverflow.com/questions/26500403/how-to-get-only-added-changed-lines-in-diff/26500491#26500491
		# Also, https://unix.stackexchange.com/a/465107
		# Move new to scan
		if [[ "$scanMode" == "scan" ]]; then	
			if [[ -f "$scanDir/.nfsScan" ]]; then
				rm "$scanDir/.nfsScan"
			fi
			mv "$scanDir/.nfsNew" "$scanDir/.nfsScan"
		fi
	fi

	# Finally, print and exit
	echo "$rString"
	return 0
}

## indexFolder()
#
# Function: Scan a folder recursively and output all underlying files to an index file
#
# Call: folderScan <folder> <index_file> [hash]
#
# Input: Above. Note: if hash is on, a SHA256 hash of the file will be taken as well
#
# Output: Stderr for any errors encountered. Otherwise just index file.
#         Returns 0 for success, 1 for failure
#
# Other: Dotfiles will be included in indices, be aware of this when giving permissions!
function indexFolder() {
	# Make sure args are correct
	if [[ -z $1 ]]; then	
		debug "l3" "No arguments given to indexFolder()! Please read documentation and try again!"
		return 1
	elif [[ -z $2 ]]; then
		debug "l3" "Too few arguments given to folderScan(), please fix and re-run!"
		return 1
	elif [[ ! -d $1 ]]; then	
		debug "l3" "First argument is not a directory! Please fix and re-run!"
		return 1
	fi
	
	# Setup
	local indexDir="$1"
	local indexFile="$2"

	if [[ -n $3 ]]; then
		debug "l1" "Enabling hash mode for indexFolder()!"
		local hashFiles=yes
	fi

	# Make sure we have access to folder and output file
	local OPWD="$(pwd)"
	if ! cd "$indexDir"; then
		debug "l3" "Could not change into $indexDir, does user have permission? Please fix and try again!"
		return 1
	elif ! touch "$indexFile"; then
		debug "l3" "Could not touch $indexFile! Does user have permission? Please fix and try again!"
		return 1
	fi

	# Time to work!
	debug "l1" "Working on indexing $indexDir!"
	local lsFile="$(pwd)/.iF-index.txt"
	local str

	if [[ -f "$lsFile" ]]; then
		rm "$lsFile" # This way the file is not counted in the index
	fi
	ls -1a > "$lsFile"

	while read -r file; do
		if [[ "$file" == . || "$file" == .. ]]; then
			true # Skip root
		elif [[ -d "$file" ]]; then
			indexFolder "$(pwd)/$file" "$indexFile" "$3"
		else
			# Assume it's a file
			str="$(pwd)/$file"
			if [[ -n $hashFiles ]]; then
				str="$str $(sha256sum "$file" | cut -d' ' -f1)"
			fi
			echo "$str" | tee -a "$indexFile" 1>/dev/null
		fi
	done < "$lsFile"
	rm "$lsFile"

	# Log and return
	debug "l1" "Done indexing folder $indexDir!"
	if ! cd "$OPWD"; then
		debug "l2" "Could not return to old directory $OPWD! Attempting to continue..."
	fi
	return 0		
}

## yesorno()
#
# Function: Returns true or false for various strings
#
# Call: yesorno <string>
#
# Input: Just the string, anything along the lines of yes/no, true/false, on/off
#
# Output: Only debug() if no argument or invalid string
#
# Other: Returns false by default, so it should be used like 'if yesorno $x; then'
function yesorno() {
	if [[ -z $1 ]]; then
		debug "l2" "No argument given to yesorno(), assuming false..."
		return 1
	fi

	local str="$(echo "$1" | awk '{print tolower($0)}')"
	case "$str" in
		y|yes|t|true|confirm|on|affirmative|positive|start|enable)
		return 0
		;;
		n|no|f|false|deny|off|negative|stop|disable|quit)
		return 1
		;;
		*)
		debug "l2" "Unknown option $str given to yesorno(), assuming false..."
		return 1
		;;
	esac
	return 1 # Just in case	
}

## isBoolean()
#
# Function: Tells whether the given string is a boolean name
#
# Input: A string of just one word
#
# Output: Stderr if no string given, otherwise nothing
#
# Returns: 0 if it is a word like "yes", "no", "y", "n", etc.
#
# Other: See function for full list
function isBoolean() {
	if [[ -z $1 ]]; then
		debug "l3" "No argument given to isBoolean()! Returning false..."
		return 1
	fi

	local str="$(echo "$1" | awk '{print tolower($0)}')"
	case "$str" in
		y|yes|t|true|confirm|on|affirmative|positive|start|enable|n|no|f|false|deny|off|negative|stop|disable|quit)
		return 0
		;;
		*)
		debug "l0" "$1 is not boolean!"
		return 1
		;;
	esac

	return 1
}

# Import the package manager common functions, but don't quit if not found
if [[ -f packageManagerCF.sh ]]; then
	source packageManagerCF.sh
elif [[ -f /usr/share/packageManagerCF.sh ]]; then
	source /usr/share/packageManagerCF.sh
else
	echo "ERROR: packageManagerCF.sh could not be located!" >&2
fi

# Finally, import the settings from commonVars.sh
if [[ -f commonVars.sh ]]; then
	source commonVars.sh
elif [[ -f /usr/share/commonVars.sh ]]; then
	source /usr/share/commonVars.sh
else
	echo "ERROR: commonVars.sh could not be found! Functions may not respond properly!" >&2
fi

# This following, which SHOULD be run in every script, will enable debugging if -v|--verbose is enabled.
# The 'shift' command lets the scripts use the rest of the arguments
if [[ "$1" == "-v" || "$1" == "--verbose" ]]; then
	export debugLevel=1
	shift
elif [[ "$1" == "-vv" || "$1" == "--vverbose" ]]; then
	export debugLevel=0
	shift
fi

#EOF