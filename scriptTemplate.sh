#!/usr/bin/env bash
#
# 
#
# Changes:
# v0.0.1
# - Initial version
#
# TODO:
#
# v0.0.1,  ,

### Setup

export longName=""
export shortName=""
#export aChar=""

if ! source /usr/share/commonFunctions.sh; then
    if ! source commonFunctions.sh; then
        echo "ERROR: commonFunctions.sh not found! Please install from https://gitlab.com/mrmusic25/linux-pref and re-run script!"
        exit 1
    fi
fi

### Variables



### Functions

function displayHelp() {
# The following will read all text between the words 'helpVar' into the variable $helpVar
# The echo at the end will output it, exactly as shown, to the user
read -d '' helpVar <<"endHelp"

.sh - Script to

Usage: ./.sh [options] 

Options:
-h | --help                         : Display this help message and exit
-v | --verbose                      : Enables verbose logging. Use -vv | --vverbose for extra debug info.



endHelp
echo "$helpVar"
}

function processArgs() {
	if [[ $# -lt 1 ]]; then
		debug "l2" "No arguments given to processArgs()! Please fix and re-run"
		return  1
	fi
	
	while [[ $loopFlag -eq 0 ]]; do
		key="$1"
			
		case "$key" in
			-h|--help)
			displayHelp
			exit 0
			;;
			-v|--verbose)
			export debugLevel=1
			debug "l1" "Verbose logging enabled"
			;;
			-vv|--vverbose)
			export debugLevel=0
			debug "l0" "Debug logging enabled"
			;;
			*)
			debug "l4" "Unknown option given: $key! Please fix and re-run!"
			return 1
			;;
		esac
		shift
	done
}

### Main Script

if ! processArgs "$@"; then
	debug "l4" "An error occurred while processing arguments, please fix and re-run!"
	displayHelp
	exit 1
fi

#EOF